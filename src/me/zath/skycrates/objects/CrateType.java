package me.zath.skycrates.objects;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.utils.GuiHolder;
import me.zath.skycrates.utils.ItemBuilder;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class CrateType {

    private String name;
    private HashMap<Prize, Integer> prizes;
    private Inventory inventory;
    private String animationName;
    private ItemStack virtualItem;

    public CrateType(String name, HashMap<Prize, Integer> prizes, Inventory inventory, String animationName, ItemStack virtualItem) {
        this.name = name;
        this.prizes = prizes;
        this.inventory = inventory;
        this.animationName = animationName;
        this.virtualItem = virtualItem;
    }

    public boolean isVirtual(){
        return virtualItem != null;
    }

    public ItemStack getVirtualItem() {
        return virtualItem;
    }

    public void setVirtualItem(ItemStack virtualItem) {
        this.virtualItem = virtualItem;
    }

    public Prize getRandomPrize() {
        int total = 0;
        for (Integer integer : prizes.values())
            total += integer;

        int index = new Random().nextInt(total);
        int sum = 0;
        int i = 0;

        while (sum < index) {
            sum += Integer.parseInt(prizes.values().toArray()[i++].toString());
        }

        return (Prize) prizes.keySet().toArray()[Math.max(0, i - 1)];
    }

    public Prize getPrize(ItemStack itemStack) {
        for (Prize prize : prizes.keySet()) {
            if (prize.getItemStack().isSimilar(itemStack))
                return prize;
        }

        return null;
    }

    public void updatePreviewGui() {
        int inventorySize = SkyCrates.getSkyCrates().getUtils().getProperSize(prizes.size());
        Inventory inventory = SkyCrates.getSkyCrates().getServer().createInventory(
            new GuiHolder(GuiHolder.TYPE.PREVIEW, null, 0), inventorySize, SkyCrates.getSkyCrates().getUtils().getConfig().getGui_PreviewName()
                .replaceAll("%crateName%", name));

        int[] fillSlots = SkyCrates.getSkyCrates().getUtils().getFillSlots(inventorySize / 9);
        for (int fillSlot : fillSlots) {
            inventory.setItem(fillSlot, SkyCrates.getSkyCrates().getUtils().getGuiItems().getFill());
        }
        prizes.keySet().forEach(prize -> {
            List<String> lore = prize.getItemStack().hasItemMeta() && prize.getItemStack().getItemMeta().hasLore()
                ? prize.getItemStack().getItemMeta().getLore() : new ArrayList<>();
            lore.add(SkyCrates.getSkyCrates().getUtils().getConfig().getGui_IconChance().replaceAll("%chance%", "" + prizes.get(prize)));

            ItemStack itemStack = new ItemBuilder(prize.getItemStack())
                .withLore(lore)
                .build();

            inventory.addItem(itemStack);
        });
        this.inventory = inventory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<Prize, Integer> getPrizes() {
        return prizes;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public String getAnimationName() {
        return animationName;
    }

    public void setAnimationName(String animationName) {
        this.animationName = animationName;
    }

}
