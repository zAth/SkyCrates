package me.zath.skycrates.objects;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.events.CrateOpenEvent;
import me.zath.skycrates.objects.animations.AbstractAnimation;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Crate {

    private CrateType crateType;
    private Location location;

    private AbstractAnimation animation;
    private Player opener;

    public Crate(CrateType crateType, Location location) {
        this.crateType = crateType;
        this.location = location;
    }

    public boolean isOccupied() {
        return animation != null && opener != null;
    }

    /**
     * returns true if was cancelled
     * */
    public boolean open(Player player) {
        CrateOpenEvent crateOpenEvent = new CrateOpenEvent(player, this);
        SkyCrates.getSkyCrates().getServer().getPluginManager().callEvent(crateOpenEvent);

        if (crateOpenEvent.isCancelled()) return true;

        opener = player;
        animation = SkyCrates.getSkyCrates().getCrateManager().getAnimation(crateType.getAnimationName(), this);
        return false;
    }

    public void endAnimation() {
        animation = null;
        opener = null;
    }

    public CrateType getCrateType() {
        return crateType;
    }

    public Location getLocation() {
        return location;
    }

    public AbstractAnimation getAnimation() {
        return animation;
    }

    public Player getOpener() {
        return opener;
    }
}
