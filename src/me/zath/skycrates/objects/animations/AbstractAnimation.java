package me.zath.skycrates.objects.animations;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.events.CrateWinEvent;
import me.zath.skycrates.objects.Crate;
import me.zath.skycrates.objects.Prize;
import org.bukkit.entity.Player;

public abstract class AbstractAnimation {

    public Crate crate;
    public Prize prize;

    public AbstractAnimation(Crate crate) {
        this.crate = crate;
        start();
    }

    public abstract void start();

    public abstract void run();

    /**
     * returns true if was cancelled
     */
    public boolean end() {
        Player opener = crate.getOpener();
        if (opener.getOpenInventory() != null)
            opener.closeInventory();

        crate.endAnimation();

        if (prize == null) return true;
        CrateWinEvent crateWinEvent = new CrateWinEvent(opener, crate, prize);
        SkyCrates.getSkyCrates().getServer().getPluginManager().callEvent(crateWinEvent);

        if (!crateWinEvent.isCancelled()) {
            prize.givePrize(opener);
            return false;
        } else {
            return true;
        }
    }

    public Crate getCrate() {
        return crate;
    }

    public Prize getPrize() {
        return prize;
    }
}
