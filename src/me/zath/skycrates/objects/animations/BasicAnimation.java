package me.zath.skycrates.objects.animations;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.objects.Crate;
import me.zath.skycrates.utils.GuiHolder;
import org.bukkit.Sound;
import org.bukkit.inventory.Inventory;

public class BasicAnimation extends AbstractAnimation {

    private Inventory inventory;
    private int count;

    public BasicAnimation(Crate crate) {
        super(crate);
    }

    @Override
    public void start() {
        count = 0;
        inventory = SkyCrates.getSkyCrates().getServer().createInventory(new GuiHolder(GuiHolder.TYPE.BASIC, null, 0), 27
            , SkyCrates.getSkyCrates().getUtils().getConfig().getGui_PreviewName().replaceAll("%crateName%", crate.getCrateType().getName()));

        for (int slot = 0; slot < inventory.getSize(); slot++)
            inventory.setItem(slot, SkyCrates.getSkyCrates().getUtils().getGuiItems().getFill());

        for (int slot = 9; slot < inventory.getSize() - 9; slot++) {
            inventory.setItem(slot, crate.getCrateType().getRandomPrize().getItemStack());
        }

        crate.getOpener().openInventory(inventory);
        wheel(20, 0);
    }

    @Override
    public void run() {
        ;
    }

    private void wheel(int maxDelay, int minDelay) {
        SkyCrates.getSkyCrates().getServer().getScheduler().scheduleAsyncDelayedTask(SkyCrates.getSkyCrates(), () -> {
            if (!crate.isOccupied() || minDelay > maxDelay) {
                SkyCrates.getSkyCrates().getServer().getScheduler().runTask(SkyCrates.getSkyCrates(), this::end);
                return;
            }

            for (int slot = 9; slot < inventory.getSize() - 10; slot++) {
                inventory.setItem(slot, inventory.getItem(slot + 1));
            }
            inventory.setItem(inventory.getSize() - 10, crate.getCrateType().getRandomPrize().getItemStack());
            prize = crate.getCrateType().getPrize(inventory.getItem(13));
            if(!crate.getCrateType().isVirtual()){
                crate.getOpener().playSound(crate.getLocation(), Sound.CLICK, 1, 1);
            } else {
                crate.getOpener().playSound(crate.getOpener().getLocation(), Sound.CLICK, 1, 1);
            }

            count++;
            int newdelay = minDelay;

            if (count >= 20) {
                newdelay = minDelay + 1;
            }

            wheel(maxDelay, newdelay);
        }, (long) minDelay);
    }

}
