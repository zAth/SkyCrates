package me.zath.skycrates.objects;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.utils.Reflection;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Prize {

    public enum TYPE {
        ITEM, CMD
    }

    private TYPE type;

    private ItemStack itemStack;
    private boolean broadcast;
    private String cmd;

    public Prize(ItemStack itemStack, boolean broadcast) {
        this.itemStack = itemStack;
        this.broadcast = broadcast;
        this.type = TYPE.ITEM;
    }

    public Prize(ItemStack itemStack, boolean broadcast, String cmd) {
        this.itemStack = itemStack;
        this.broadcast = broadcast;
        this.cmd = cmd;
        this.type = TYPE.CMD;
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public boolean isBroadcast() {
        return broadcast;
    }

    public void setBroadcast(boolean broadcast) {
        this.broadcast = broadcast;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public void givePrize(Player player) {
        if (type == TYPE.ITEM) {
            if (player.getInventory().firstEmpty() == -1) {
                player.getWorld().dropItemNaturally(player.getLocation(), itemStack);
            } else {
                player.getInventory().addItem(itemStack);
            }
        } else if (type == TYPE.CMD) {
            SkyCrates.getSkyCrates().getServer().dispatchCommand(SkyCrates.getSkyCrates().getServer().getConsoleSender()
                , cmd.replaceAll("%p%", player.getName()));
        }

        if (broadcast) {
            for (Player onlinePlayer : SkyCrates.getSkyCrates().getServer().getOnlinePlayers()) {
                String itemStackName = itemStack.hasItemMeta() && itemStack.getItemMeta().hasDisplayName()
                    ? itemStack.getItemMeta().getDisplayName() : SkyCrates.getSkyCrates().getUtils().prettifyText(itemStack.getType().name());

                String message = SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_Broadcast()
                    .replaceAll("%playerName%", player.getName())
                    .replaceAll("%itemStack%", itemStackName);

                sendItemTooltipMessage(onlinePlayer, message, itemStack);
            }
        }
    }

    private void sendItemTooltipMessage(Player player, String msg, ItemStack itemStack) {
        Reflection reflection = SkyCrates.getSkyCrates().getUtils().getReflection();

        Object nbtTagCompound;
        Object nmsItem;
        Object jsonItem;

        try {
            nbtTagCompound = reflection.getClass_NBTTagCompound().newInstance();
            nmsItem = reflection.getMethod_asNMSCopy().invoke(null, itemStack);
            jsonItem = reflection.getMethod_SaveItem().invoke(nmsItem, nbtTagCompound);
        } catch (Throwable t) {
            jsonItem = null;

            SkyCrates.getSkyCrates().getServer().getConsoleSender().sendMessage("Failed to serialize itemstack to nms item");
            t.printStackTrace();
        }

        String json = jsonItem == null ? "" : jsonItem.toString();

        BaseComponent[] baseComponents = new BaseComponent[]{
            new TextComponent(json)
        };

        HoverEvent hoverEvent = new HoverEvent(HoverEvent.Action.SHOW_ITEM, baseComponents);
        TextComponent textComponent = new TextComponent(msg);
        textComponent.setHoverEvent(hoverEvent);

        player.spigot().sendMessage(textComponent);
    }
}
