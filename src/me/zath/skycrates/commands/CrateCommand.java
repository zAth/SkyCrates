package me.zath.skycrates.commands;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.commands.sub.*;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

import java.util.HashMap;
import java.util.Map;

public class CrateCommand implements CommandExecutor {

    private Map<String, ICommand> cmds = new HashMap<>();
    private String format,noConsole,noPermission,noCommand;

    public CrateCommand() {
        format = SkyCrates.getSkyCrates().getUtils().getConfig().getString("Cmds.Format").replaceAll("&", "§");
        noConsole = SkyCrates.getSkyCrates().getUtils().getConfig().getString("Cmds.NoConsole").replaceAll("&", "§");
        noPermission = SkyCrates.getSkyCrates().getUtils().getConfig().getString("Cmds.NoPermission").replaceAll("&", "§");
        noCommand = SkyCrates.getSkyCrates().getUtils().getConfig().getString("Cmds.NoCommand").replaceAll("&", "§");

        cmds.put("give", new Give());
        cmds.put("set", new Set());
        cmds.put("locations", new Locations());
        cmds.put("create", new Create());
        cmds.put("edit", new Edit());
        cmds.put("reload", new Reload());
    }

    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
        if(cmd.getName().equalsIgnoreCase("crate")) {
            if (args.length == 0) {
                for (String entry : cmds.keySet()) {
                    sender.sendMessage(format.replaceAll("%cmd%", "crate " + entry).replaceAll("%descricao%", cmds.get(entry).getDescription()).replaceAll("&", "§"));
                }
            } else {
                ICommand icmd = cmds.get(args[0].toLowerCase());
                if (icmd != null) {
                    if (!icmd.supportsConsole() && (sender instanceof ConsoleCommandSender)) {
                        sender.sendMessage(noConsole);
                        return true;
                    }
                    if (!sender.hasPermission(icmd.getPermission())) {
                        sender.sendMessage(noPermission);
                        return true;
                    }
                    icmd.execute(sender, args, SkyCrates.getSkyCrates());
                    return true;
                } else {
                    sender.sendMessage(noCommand);
                    return true;
                }
            }
        }
        return false;
    }

}
