package me.zath.skycrates.commands;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import org.bukkit.command.CommandSender;

public abstract class AbstractCommand implements ICommand {

    private String description;

    public AbstractCommand() {
        description = SkyCrates.getSkyCrates().getUtils().getConfig().getString("Cmds." + this.getClass().getSimpleName());
    }

    @Override
    public abstract void execute(CommandSender sender, String[] args, SkyCrates skyCrates);

    @Override
    public abstract boolean supportsConsole();

    @Override
    public String getPermission(){
        return "skycrates.command." + this.getClass().getSimpleName().toLowerCase();
    }

    @Override
    public String getDescription() {
        return description;
    }
}
