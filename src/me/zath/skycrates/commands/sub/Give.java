package me.zath.skycrates.commands.sub;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.commands.AbstractCommand;
import me.zath.skycrates.objects.CrateType;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Give extends AbstractCommand {

    @Override
    public void execute(CommandSender sender, String[] args, SkyCrates skyCrates) {
        if (args.length != 3) {
            sender.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_DefineCrateType()
                .replaceAll("%crateTypes%", String.join(", ", SkyCrates.getSkyCrates().getCrateManager().getCrateTypesNames())));

            sender.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_DefineTarget());
            return;
        }

        String crateTypeName = args[1];
        if (!SkyCrates.getSkyCrates().getCrateManager().isCrateType(crateTypeName)) {
            sender.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_NoCrateType()
                .replaceAll("%crateTypes%", String.join(", ", SkyCrates.getSkyCrates().getCrateManager().getCrateTypesNames())));

            return;
        }
        CrateType crateType = SkyCrates.getSkyCrates().getCrateManager().getCrateType(crateTypeName);
        ItemStack itemStack = crateType.isVirtual() ? crateType.getVirtualItem()
            : SkyCrates.getSkyCrates().getUtils().getGuiItems().getKey(crateType.getName());

        String targetName = args[2];
        if (targetName.equals("*")) {
            SkyCrates.getSkyCrates().getServer().getOnlinePlayers().forEach(player -> {
                if (player.getInventory().firstEmpty() == -1) {
                    player.getWorld().dropItemNaturally(player.getLocation(), itemStack);
                } else {
                    player.getInventory().addItem(itemStack);
                }
            });
        } else {
            Player player = SkyCrates.getSkyCrates().getUtils().getPlayer(targetName);
            if (player == null) {
                sender.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_NoTarget());
                return;
            }

            if (player.getInventory().firstEmpty() == -1) {
                player.getWorld().dropItemNaturally(player.getLocation(), itemStack);
            } else {
                player.getInventory().addItem(itemStack);
            }
        }
    }

    @Override
    public boolean supportsConsole() {
        return true;
    }

}
