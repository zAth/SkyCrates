package me.zath.skycrates.commands.sub;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.commands.AbstractCommand;
import me.zath.skycrates.inventories.EditGUI;
import me.zath.skycrates.objects.CrateType;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Create extends AbstractCommand {

    @Override
    public void execute(CommandSender sender, String[] args, SkyCrates skyCrates) {
        Player player = (Player) sender;

        if (args.length != 3) {
            sender.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_DefineName());

            sender.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_DefineAnimation()
                .replaceAll("%animations%", String.join(", ", SkyCrates.getSkyCrates().getCrateManager().getAnimationNames())));

            return;
        }

        String name = args[1];
        if (SkyCrates.getSkyCrates().getCrateManager().isCrateType(name)) {
            player.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_AlreadyCrateType());
            return;
        }

        String animationName = args[2];
        if (!SkyCrates.getSkyCrates().getCrateManager().isAnimation(animationName)) {
            player.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_NoAnimation());
            return;
        }
        CrateType crateType = SkyCrates.getSkyCrates().getCrateManager().createCrateType(name, animationName);

        player.openInventory(new EditGUI(crateType).getInventory());
        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
    }

    @Override
    public boolean supportsConsole() {
        return false;
    }

}
