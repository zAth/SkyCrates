package me.zath.skycrates.commands.sub;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.commands.AbstractCommand;
import me.zath.skycrates.inventories.EditGUI;
import me.zath.skycrates.objects.CrateType;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Edit extends AbstractCommand {

    @Override
    public void execute(CommandSender sender, String[] args, SkyCrates skyCrates) {
        Player player = (Player) sender;

        if (args.length != 2) {
            sender.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_DefineCrateType()
                .replaceAll("%crateTypes%", String.join(", ", SkyCrates.getSkyCrates().getCrateManager().getCrateTypesNames())));

            return;
        }

        String crateTypeName = args[1];
        if (!SkyCrates.getSkyCrates().getCrateManager().isCrateType(crateTypeName)) {
            sender.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_NoCrateType()
                .replaceAll("%crateTypes%", String.join(", ", SkyCrates.getSkyCrates().getCrateManager().getCrateTypesNames())));

            return;
        }
        CrateType crateType = SkyCrates.getSkyCrates().getCrateManager().getCrateType(crateTypeName);

        player.openInventory(new EditGUI(crateType).getInventory());
        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
    }

    @Override
    public boolean supportsConsole() {
        return false;
    }

}
