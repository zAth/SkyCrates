package me.zath.skycrates.commands.sub;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.commands.AbstractCommand;
import me.zath.skycrates.objects.CrateType;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;

public class Set extends AbstractCommand {

    @Override
    public void execute(CommandSender sender, String[] args, SkyCrates skyCrates) {
        if(args.length != 2){
            sender.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_DefineCrateType()
                .replaceAll("%crateTypes%", String.join(", ", SkyCrates.getSkyCrates().getCrateManager().getCrateTypesNames())));

            return;
        }

        String crateTypeName = args[1];
        if(!SkyCrates.getSkyCrates().getCrateManager().isCrateType(crateTypeName)){
            sender.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_NoCrateType()
                .replaceAll("%crateTypes%", String.join(", ", SkyCrates.getSkyCrates().getCrateManager().getCrateTypesNames())));

            return;
        }
        CrateType crateType = SkyCrates.getSkyCrates().getCrateManager().getCrateType(crateTypeName);

        Player player = (Player) sender;

        java.util.Set<Material> set = new HashSet<>();
        set.add(Material.AIR);
        Block block = player.getTargetBlock(set, 100);

        if(block == null || block.getType() == Material.AIR){
            player.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_NoBlock());
            return;
        }

        if(SkyCrates.getSkyCrates().getCrateManager().isCrate(block.getLocation())){
            player.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_AlreadyCrate());
            return;
        }

        if(crateType.isVirtual()){
            player.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_SetVirtual());
            return;
        }

        SkyCrates.getSkyCrates().getCrateManager().createCrate(block.getLocation(), crateType);
        player.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_CrateCreated());
    }

    @Override
    public boolean supportsConsole() {
        return false;
    }

}
