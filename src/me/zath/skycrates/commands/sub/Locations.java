package me.zath.skycrates.commands.sub;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.commands.AbstractCommand;
import org.bukkit.command.CommandSender;

public class Locations extends AbstractCommand {

    @Override
    public void execute(CommandSender sender, String[] args, SkyCrates skyCrates) {
        SkyCrates.getSkyCrates().getCrateManager().getCrates().forEach(crate ->
            sender.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_Location()
                .replaceAll("%serializedLocation%", SkyCrates.getSkyCrates().getUtils().toString(crate.getLocation()))
                .replaceAll("%crateName%", crate.getCrateType().getName())
            )
        );
    }

    @Override
    public boolean supportsConsole() {
        return true;
    }

}
