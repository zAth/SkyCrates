package me.zath.skycrates.commands.sub;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.commands.AbstractCommand;
import org.bukkit.command.CommandSender;

public class Reload extends AbstractCommand {

    @Override
    public void execute(CommandSender sender, String[] args, SkyCrates skyCrates) {
        SkyCrates.getSkyCrates().getUtils().reload();
        sender.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_Reload());
    }

    @Override
    public boolean supportsConsole() {
        return true;
    }

}
