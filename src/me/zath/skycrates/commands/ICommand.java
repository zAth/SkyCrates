package me.zath.skycrates.commands;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import org.bukkit.command.CommandSender;

public interface ICommand {

    void execute(CommandSender sender, String[] args, SkyCrates skyCrates);

    boolean supportsConsole();

    String getPermission();

    String getDescription();
}
