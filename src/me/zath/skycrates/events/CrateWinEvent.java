package me.zath.skycrates.events;
/*
 * MC
 * Created by zAth
 */

import me.zath.skycrates.objects.Crate;
import me.zath.skycrates.objects.Prize;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CrateWinEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;

    private Player player;
    private Crate crate;
    private Prize prize;

    public CrateWinEvent(Player player, Crate crate, Prize prize) {
        cancelled = false;
        this.player = player;
        this.crate = crate;
        this.prize = prize;
    }

    public Player getPlayer() {
        return player;
    }

    public Crate getCrate() {
        return crate;
    }

    public Prize getPrize() {
        return prize;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
