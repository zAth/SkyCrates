package me.zath.skycrates;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.commands.CrateCommand;
import me.zath.skycrates.listeners.BlockListener;
import me.zath.skycrates.listeners.InventoryListener;
import me.zath.skycrates.managers.CrateManager;
import me.zath.skycrates.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class SkyCrates extends JavaPlugin {

    // TODO: 03-02-2018
    // crate holograms and particles

    private static SkyCrates skyCrates;
    private Utils utils;
    private CrateManager crateManager;

    @Override
    public void onEnable() {
        skyCrates = this;
        File file = new File(getDataFolder(), "config.yml");
        if (!(file.exists())) {
            try {
                saveResource("config.yml", false);
            } catch (Exception ignored) {
            }
        }
        saveDefaultConfig();
        utils = new Utils();
        utils.updateConfig(this, "config.yml", "config.yml");
        crateManager = new CrateManager();
        registerCommands();
        registerEvents();
        Bukkit.getConsoleSender().sendMessage("§6<§8-----------------------------§6>");
        Bukkit.getConsoleSender().sendMessage("§6" + getDescription().getName() + " §8v.§6" + getDescription().getVersion() + " §8de§6 "
            + getDescription().getAuthors() + " §2Ativado");
        Bukkit.getConsoleSender().sendMessage("§6<§8-----------------------------§6>");
    }

    @Override
    public void onDisable() {
        Bukkit.getConsoleSender().sendMessage("§6<§8-----------------------------§6>");
        Bukkit.getConsoleSender().sendMessage("§6" + getDescription().getName() + " §8v.§6" + getDescription().getVersion() + " §8de§6 "
            + getDescription().getAuthors() + " §4Desativado");
        Bukkit.getConsoleSender().sendMessage("§6<§8-----------------------------§6>");
        HandlerList.unregisterAll();
    }

    private void registerCommands() {
        getServer().getPluginCommand("crate").setExecutor(new CrateCommand());
    }

    private void registerEvents() {
        getServer().getPluginManager().registerEvents(new BlockListener(), this);
        getServer().getPluginManager().registerEvents(new InventoryListener(), this);
    }

    public static SkyCrates getSkyCrates() {
        return skyCrates;
    }

    public Utils getUtils() {
        return utils;
    }

    public CrateManager getCrateManager() {
        return crateManager;
    }
}
