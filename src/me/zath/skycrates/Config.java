package me.zath.skycrates;
/*
 * MC
 * Created by zAth
 */

import org.bukkit.configuration.ConfigurationSection;

import java.util.List;
import java.util.stream.Collectors;

public class Config {

    private String gui_PreviewName, msg_Location, msg_NoPerm, msg_DefineCrateType, msg_NoCrateType, msg_NoBlock, msg_AlreadyCrate
        , msg_CrateCreated, msg_DefineTarget, msg_NoTarget, msg_DefineName, msg_DefineAnimation, msg_AlreadyCrateType, msg_NoAnimation
        , gui_IconChance, msg_Occupied, msg_Broadcast, msg_SetVirtual, msg_Reload, msg_HoldVirtualItem;

    public Config() {
        gui_PreviewName = getString("Gui.PreviewName").replaceAll("&", "§");
        gui_IconChance = getString("Gui.IconChance").replaceAll("&", "§");
        msg_Reload = getString("Msg.Reload").replaceAll("&", "§");
        msg_SetVirtual = getString("Msg.SetVirtual").replaceAll("&", "§");
        msg_Broadcast = getString("Msg.Broadcast").replaceAll("&", "§");
        msg_Occupied = getString("Msg.Occupied").replaceAll("&", "§");
        msg_Location = getString("Msg.Location").replaceAll("&", "§");
        msg_NoPerm = getString("Msg.NoPerm").replaceAll("&", "§");
        msg_DefineCrateType = getString("Msg.DefineCrateType").replaceAll("&", "§");
        msg_DefineTarget = getString("Msg.DefineTarget").replaceAll("&", "§");
        msg_NoCrateType = getString("Msg.NoCrateType").replaceAll("&", "§");
        msg_NoBlock = getString("Msg.NoBlock").replaceAll("&", "§");
        msg_NoTarget = getString("Msg.NoTarget").replaceAll("&", "§");
        msg_AlreadyCrate = getString("Msg.AlreadyCrate").replaceAll("&", "§");
        msg_CrateCreated = getString("Msg.CrateCreated").replaceAll("&", "§");
        msg_DefineName = getString("Msg.DefineName").replaceAll("&", "§");
        msg_DefineAnimation = getString("Msg.DefineAnimation").replaceAll("&", "§");
        msg_AlreadyCrateType = getString("Msg.AlreadyCrateType").replaceAll("&", "§");
        msg_NoAnimation = getString("Msg.NoAnimation").replaceAll("&", "§");
        msg_HoldVirtualItem = getString("Msg.HoldVirtualItem").replaceAll("&", "§");
    }

    public String getMsg_HoldVirtualItem() {
        return msg_HoldVirtualItem;
    }

    public String getMsg_Reload() {
        return msg_Reload;
    }

    public String getMsg_SetVirtual() {
        return msg_SetVirtual;
    }

    public String getMsg_Broadcast() {
        return msg_Broadcast;
    }

    public String getMsg_Occupied() {
        return msg_Occupied;
    }

    public String getGui_IconChance() {
        return gui_IconChance;
    }

    public String getMsg_DefineName() {
        return msg_DefineName;
    }

    public String getMsg_DefineAnimation() {
        return msg_DefineAnimation;
    }

    public String getMsg_AlreadyCrateType() {
        return msg_AlreadyCrateType;
    }

    public String getMsg_NoAnimation() {
        return msg_NoAnimation;
    }

    public String getMsg_NoTarget() {
        return msg_NoTarget;
    }

    public String getMsg_DefineTarget() {
        return msg_DefineTarget;
    }

    public String getGui_PreviewName() {
        return gui_PreviewName;
    }

    public String getMsg_Location() {
        return msg_Location;
    }

    public String getMsg_NoPerm() {
        return msg_NoPerm;
    }

    public String getMsg_DefineCrateType() {
        return msg_DefineCrateType;
    }

    public String getMsg_NoCrateType() {
        return msg_NoCrateType;
    }

    public String getMsg_NoBlock() {
        return msg_NoBlock;
    }

    public String getMsg_AlreadyCrate() {
        return msg_AlreadyCrate;
    }

    public String getMsg_CrateCreated() {
        return msg_CrateCreated;
    }

    public void set(String path, Object value) {
        try {
            SkyCrates.getSkyCrates().getConfig().set(path, value);
            SkyCrates.getSkyCrates().saveConfig();
        } catch (Exception e) {
            SkyCrates.getSkyCrates().getServer().getPluginManager().disablePlugin(SkyCrates.getSkyCrates());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
    }


    public ConfigurationSection getConfigurationSection(String path) {
        ConfigurationSection toReturn;
        try {
            toReturn = SkyCrates.getSkyCrates().getConfig().getConfigurationSection(path);
        } catch (Exception e) {
            SkyCrates.getSkyCrates().getServer().getPluginManager().disablePlugin(SkyCrates.getSkyCrates());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }

    public Boolean getBoolean(String path) {
        Boolean toReturn;
        try {
            toReturn = SkyCrates.getSkyCrates().getConfig().getBoolean(path);
        } catch (Exception e) {
            SkyCrates.getSkyCrates().getServer().getPluginManager().disablePlugin(SkyCrates.getSkyCrates());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }

    public String getString(String path) {
        String toReturn;
        try {
            toReturn = SkyCrates.getSkyCrates().getConfig().getString(path);
        } catch (Exception e) {
            SkyCrates.getSkyCrates().getServer().getPluginManager().disablePlugin(SkyCrates.getSkyCrates());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }

    public int getInt(String path) {
        int toReturn;
        try {
            toReturn = SkyCrates.getSkyCrates().getConfig().getInt(path);
        } catch (Exception e) {
            SkyCrates.getSkyCrates().getServer().getPluginManager().disablePlugin(SkyCrates.getSkyCrates());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }

    public double getDouble(String path) {
        Double toReturn;
        try {
            toReturn = SkyCrates.getSkyCrates().getConfig().getDouble(path);
        } catch (Exception e) {
            SkyCrates.getSkyCrates().getServer().getPluginManager().disablePlugin(SkyCrates.getSkyCrates());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }

    public List<String> getList(String path) {
        List<String> toReturn;
        try {
            toReturn = SkyCrates.getSkyCrates().getConfig().getStringList(path);
        } catch (Exception e) {
            SkyCrates.getSkyCrates().getServer().getPluginManager().disablePlugin(SkyCrates.getSkyCrates());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }

    public List<String> makeListColory(List<String> l) {
        return l.stream().map(str -> str.replaceAll("&", "§")).collect(Collectors.toList());
    }

    public boolean exists(String path) {
        boolean toReturn;
        try {
            toReturn = SkyCrates.getSkyCrates().getConfig().get(path) != null;
        } catch (Exception e) {
            SkyCrates.getSkyCrates().getServer().getPluginManager().disablePlugin(SkyCrates.getSkyCrates());
            throw new IllegalArgumentException("Invalid config: " + e.getMessage() + "\n");
        }
        return toReturn;
    }
}
