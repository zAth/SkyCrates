package me.zath.skycrates.utils;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class GuiItems {

    private ItemStack fill, key, command, chance, delete, name, animation, broadcast, changeKey, changeNoKey, virtualCrate, writeIdData
        , nextPage, previousPage, open, close;

    public GuiItems() {
        fill = new ItemBuilder(SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Fill.Id"), SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Fill.Data"))
            .withName(SkyCrates.getSkyCrates().getUtils().getConfig().getString("Gui.Items.Fill.Name").replaceAll("&", "§"))
            .withLore(SkyCrates.getSkyCrates().getUtils().getConfig().makeListColory(SkyCrates.getSkyCrates().getUtils().getConfig().getList("Gui.Items.Fill.Lore")))
            .build();

        key = new ItemBuilder(SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Key.Id"), SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Key.Data"))
            .withName(SkyCrates.getSkyCrates().getUtils().getConfig().getString("Gui.Items.Key.Name").replaceAll("&", "§"))
            .withLore(SkyCrates.getSkyCrates().getUtils().getConfig().makeListColory(SkyCrates.getSkyCrates().getUtils().getConfig().getList("Gui.Items.Key.Lore")))
            .withGlow(SkyCrates.getSkyCrates().getUtils().getConfig().getBoolean("Gui.Items.Key.Glow"))
            .build();

        virtualCrate = new ItemBuilder(SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.VirtualCrate.Id"), SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.VirtualCrate.Data"))
            .withName(SkyCrates.getSkyCrates().getUtils().getConfig().getString("Gui.Items.VirtualCrate.Name").replaceAll("&", "§"))
            .withLore(SkyCrates.getSkyCrates().getUtils().getConfig().makeListColory(SkyCrates.getSkyCrates().getUtils().getConfig().getList("Gui.Items.VirtualCrate.Lore")))
            .withGlow(SkyCrates.getSkyCrates().getUtils().getConfig().getBoolean("Gui.Items.VirtualCrate.Glow"))
            .build();

        command = new ItemBuilder(SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Command.Id"), SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Command.Data"))
            .withName(SkyCrates.getSkyCrates().getUtils().getConfig().getString("Gui.Items.Command.Name").replaceAll("&", "§"))
            .withLore(SkyCrates.getSkyCrates().getUtils().getConfig().makeListColory(SkyCrates.getSkyCrates().getUtils().getConfig().getList("Gui.Items.Command.Lore")))
            .withGlow(SkyCrates.getSkyCrates().getUtils().getConfig().getBoolean("Gui.Items.Command.Glow"))
            .build();

        chance = new ItemBuilder(SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Chance.Id"), SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Chance.Data"))
            .withName(SkyCrates.getSkyCrates().getUtils().getConfig().getString("Gui.Items.Chance.Name").replaceAll("&", "§"))
            .withLore(SkyCrates.getSkyCrates().getUtils().getConfig().makeListColory(SkyCrates.getSkyCrates().getUtils().getConfig().getList("Gui.Items.Chance.Lore")))
            .withGlow(SkyCrates.getSkyCrates().getUtils().getConfig().getBoolean("Gui.Items.Chance.Glow"))
            .build();

        delete = new ItemBuilder(SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Delete.Id"), SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Delete.Data"))
            .withName(SkyCrates.getSkyCrates().getUtils().getConfig().getString("Gui.Items.Delete.Name").replaceAll("&", "§"))
            .withLore(SkyCrates.getSkyCrates().getUtils().getConfig().makeListColory(SkyCrates.getSkyCrates().getUtils().getConfig().getList("Gui.Items.Delete.Lore")))
            .withGlow(SkyCrates.getSkyCrates().getUtils().getConfig().getBoolean("Gui.Items.Delete.Glow"))
            .build();

        name = new ItemBuilder(SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Name.Id"), SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Name.Data"))
            .withName(SkyCrates.getSkyCrates().getUtils().getConfig().getString("Gui.Items.Name.Name").replaceAll("&", "§"))
            .withLore(SkyCrates.getSkyCrates().getUtils().getConfig().makeListColory(SkyCrates.getSkyCrates().getUtils().getConfig().getList("Gui.Items.Name.Lore")))
            .withGlow(SkyCrates.getSkyCrates().getUtils().getConfig().getBoolean("Gui.Items.Name.Glow"))
            .build();

        animation = new ItemBuilder(SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Animation.Id"), SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Animation.Data"))
            .withName(SkyCrates.getSkyCrates().getUtils().getConfig().getString("Gui.Items.Animation.Name").replaceAll("&", "§"))
            .withLore(SkyCrates.getSkyCrates().getUtils().getConfig().makeListColory(SkyCrates.getSkyCrates().getUtils().getConfig().getList("Gui.Items.Animation.Lore")))
            .withGlow(SkyCrates.getSkyCrates().getUtils().getConfig().getBoolean("Gui.Items.Animation.Glow"))
            .build();

        broadcast = new ItemBuilder(SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Broadcast.Id"), SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Broadcast.Data"))
            .withName(SkyCrates.getSkyCrates().getUtils().getConfig().getString("Gui.Items.Broadcast.Name").replaceAll("&", "§"))
            .withLore(SkyCrates.getSkyCrates().getUtils().getConfig().makeListColory(SkyCrates.getSkyCrates().getUtils().getConfig().getList("Gui.Items.Broadcast.Lore")))
            .withGlow(SkyCrates.getSkyCrates().getUtils().getConfig().getBoolean("Gui.Items.Broadcast.Glow"))
            .build();

        changeKey = new ItemBuilder(SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.ChangeKey.Id"), SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.ChangeKey.Data"))
            .withName(SkyCrates.getSkyCrates().getUtils().getConfig().getString("Gui.Items.ChangeKey.Name").replaceAll("&", "§"))
            .withLore(SkyCrates.getSkyCrates().getUtils().getConfig().makeListColory(SkyCrates.getSkyCrates().getUtils().getConfig().getList("Gui.Items.ChangeKey.Lore")))
            .withGlow(SkyCrates.getSkyCrates().getUtils().getConfig().getBoolean("Gui.Items.ChangeKey.Glow"))
            .build();

        changeNoKey = new ItemBuilder(SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.ChangeNoKey.Id"), SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.ChangeNoKey.Data"))
            .withName(SkyCrates.getSkyCrates().getUtils().getConfig().getString("Gui.Items.ChangeNoKey.Name").replaceAll("&", "§"))
            .withLore(SkyCrates.getSkyCrates().getUtils().getConfig().makeListColory(SkyCrates.getSkyCrates().getUtils().getConfig().getList("Gui.Items.ChangeNoKey.Lore")))
            .withGlow(SkyCrates.getSkyCrates().getUtils().getConfig().getBoolean("Gui.Items.ChangeNoKey.Glow"))
            .build();

        writeIdData = new ItemBuilder(SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.WriteIdData.Id"), SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.WriteIdData.Data"))
            .withName(SkyCrates.getSkyCrates().getUtils().getConfig().getString("Gui.Items.WriteIdData.Name").replaceAll("&", "§"))
            .withLore(SkyCrates.getSkyCrates().getUtils().getConfig().makeListColory(SkyCrates.getSkyCrates().getUtils().getConfig().getList("Gui.Items.WriteIdData.Lore")))
            .withGlow(SkyCrates.getSkyCrates().getUtils().getConfig().getBoolean("Gui.Items.WriteIdData.Glow"))
            .build();

        nextPage = new ItemBuilder(SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.NextPage.Id"), SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.NextPage.Data"))
            .withName(SkyCrates.getSkyCrates().getUtils().getConfig().getString("Gui.Items.NextPage.Name").replaceAll("&", "§"))
            .withLore(SkyCrates.getSkyCrates().getUtils().getConfig().makeListColory(SkyCrates.getSkyCrates().getUtils().getConfig().getList("Gui.Items.NextPage.Lore")))
            .withGlow(SkyCrates.getSkyCrates().getUtils().getConfig().getBoolean("Gui.Items.NextPage.Glow"))
            .build();

        previousPage = new ItemBuilder(SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.PreviousPage.Id"), SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.PreviousPage.Data"))
            .withName(SkyCrates.getSkyCrates().getUtils().getConfig().getString("Gui.Items.PreviousPage.Name").replaceAll("&", "§"))
            .withLore(SkyCrates.getSkyCrates().getUtils().getConfig().makeListColory(SkyCrates.getSkyCrates().getUtils().getConfig().getList("Gui.Items.PreviousPage.Lore")))
            .withGlow(SkyCrates.getSkyCrates().getUtils().getConfig().getBoolean("Gui.Items.PreviousPage.Glow"))
            .build();

        open = new ItemBuilder(SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Open.Id"), SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Open.Data"))
            .withName(SkyCrates.getSkyCrates().getUtils().getConfig().getString("Gui.Items.Open.Name").replaceAll("&", "§"))
            .withLore(SkyCrates.getSkyCrates().getUtils().getConfig().makeListColory(SkyCrates.getSkyCrates().getUtils().getConfig().getList("Gui.Items.Open.Lore")))
            .withGlow(SkyCrates.getSkyCrates().getUtils().getConfig().getBoolean("Gui.Items.Open.Glow"))
            .build();

        close = new ItemBuilder(SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Close.Id"), SkyCrates.getSkyCrates().getUtils().getConfig().getInt("Gui.Items.Close.Data"))
            .withName(SkyCrates.getSkyCrates().getUtils().getConfig().getString("Gui.Items.Close.Name").replaceAll("&", "§"))
            .withLore(SkyCrates.getSkyCrates().getUtils().getConfig().makeListColory(SkyCrates.getSkyCrates().getUtils().getConfig().getList("Gui.Items.Close.Lore")))
            .withGlow(SkyCrates.getSkyCrates().getUtils().getConfig().getBoolean("Gui.Items.Close.Glow"))
            .build();
    }

    public ItemStack getOpen() {
        return open;
    }

    public ItemStack getClose() {
        return close;
    }

    public ItemStack getNextPage() {
        return nextPage;
    }

    public ItemStack getPreviousPage() {
        return previousPage;
    }

    public ItemStack getWriteIdData() {
        return writeIdData;
    }

    public ItemStack getVirtualCrate(String crateName, int id, int data) {
        return new ItemBuilder(virtualCrate)
            .setIdData(id, (byte) data)
            .withLore(virtualCrate.getItemMeta().getLore().stream()
                .map(string -> string.replaceAll("%crateName%", crateName))
                .collect(Collectors.toList()))
            .build();
    }

    public ItemStack getChangeNoKey() {
        return changeNoKey;
    }

    public ItemStack getChangeKey() {
        return changeKey;
    }

    public ItemStack getBroadcast() {
        return broadcast;
    }

    public ItemStack getName() {
        return name;
    }

    public ItemStack getAnimation() {
        return animation;
    }

    public ItemStack getDelete() {
        return delete;
    }

    public ItemStack getChance() {
        return chance;
    }

    public ItemStack getCommand() {
        return command;
    }

    public ItemStack getFill() {
        return fill;
    }

    public ItemStack getKey(String crateName) {
        return new ItemBuilder(key)
            .withLore(key.getItemMeta().getLore().stream()
                .map(string -> string.replaceAll("%crateName%", crateName))
                .collect(Collectors.toList()))
            .build();
    }

    public ArrayList<ItemStack> getIcons() {
        ArrayList<Material> unavailableMaterials = new ArrayList<>();

        unavailableMaterials.add(Material.AIR);
        unavailableMaterials.add(Material.WATER);
        unavailableMaterials.add(Material.STATIONARY_WATER);
        unavailableMaterials.add(Material.LAVA);
        unavailableMaterials.add(Material.STATIONARY_LAVA);
        unavailableMaterials.add(Material.BED_BLOCK);
        unavailableMaterials.add(Material.PISTON_EXTENSION);
        unavailableMaterials.add(Material.PISTON_MOVING_PIECE);
        unavailableMaterials.add(Material.DOUBLE_STEP);
        unavailableMaterials.add(Material.FIRE);
        unavailableMaterials.add(Material.REDSTONE_WIRE);
        unavailableMaterials.add(Material.CROPS);
        unavailableMaterials.add(Material.SIGN_POST);
        unavailableMaterials.add(Material.WOODEN_DOOR);
        unavailableMaterials.add(Material.WALL_SIGN);
        unavailableMaterials.add(Material.IRON_DOOR_BLOCK);
        unavailableMaterials.add(Material.GLOWING_REDSTONE_ORE);
        unavailableMaterials.add(Material.REDSTONE_TORCH_OFF);
        unavailableMaterials.add(Material.SUGAR_CANE_BLOCK);
        unavailableMaterials.add(Material.PORTAL);
        unavailableMaterials.add(Material.CAKE_BLOCK);
        unavailableMaterials.add(Material.DIODE_BLOCK_OFF);
        unavailableMaterials.add(Material.DIODE_BLOCK_ON);
        unavailableMaterials.add(Material.PUMPKIN_STEM);
        unavailableMaterials.add(Material.MELON_STEM);
        unavailableMaterials.add(Material.NETHER_WARTS);
        unavailableMaterials.add(Material.BREWING_STAND);
        unavailableMaterials.add(Material.CAULDRON);
        unavailableMaterials.add(Material.ENDER_PORTAL);
        unavailableMaterials.add(Material.REDSTONE_LAMP_ON);
        unavailableMaterials.add(Material.WOOD_DOUBLE_STEP);
        unavailableMaterials.add(Material.COCOA);
        unavailableMaterials.add(Material.TRIPWIRE);
        unavailableMaterials.add(Material.FLOWER_POT);
        unavailableMaterials.add(Material.CARROT);
        unavailableMaterials.add(Material.POTATO);
        unavailableMaterials.add(Material.SKULL);
        unavailableMaterials.add(Material.REDSTONE_COMPARATOR_ON);
        unavailableMaterials.add(Material.REDSTONE_COMPARATOR_OFF);
        unavailableMaterials.add(Material.STANDING_BANNER);
        unavailableMaterials.add(Material.WALL_BANNER);
        unavailableMaterials.add(Material.DAYLIGHT_DETECTOR_INVERTED);
        unavailableMaterials.add(Material.DOUBLE_STONE_SLAB2);
        unavailableMaterials.add(Material.SPRUCE_DOOR);
        unavailableMaterials.add(Material.BIRCH_DOOR);
        unavailableMaterials.add(Material.JUNGLE_DOOR);
        unavailableMaterials.add(Material.ACACIA_DOOR);
        unavailableMaterials.add(Material.DARK_OAK_DOOR);

        ArrayList<ItemStack> toReturn = new ArrayList<>();

        for (Material material : Material.values()) {
            if (unavailableMaterials.contains(material)) continue;

            ItemStack itemStack;
            switch (material) {
                case STONE:
                    for(int data = 0; data <= 6; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case DIRT:
                    for(int data = 0; data <= 2; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case WOOD:
                    for(int data = 0; data <= 5; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case SAPLING:
                    for(int data = 0; data <= 5; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case SAND:
                    for(int data = 0; data <= 1; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case LOG:
                    for(int data = 0; data <= 3; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case LEAVES:
                    for(int data = 0; data <= 3; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case SPONGE:
                    for(int data = 0; data <= 1; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case SANDSTONE:
                    for(int data = 0; data <= 2; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case LONG_GRASS:
                    for(int data = 0; data <= 2; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case QUARTZ_BLOCK:
                    for(int data = 0; data <= 2; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case STAINED_CLAY:
                    for(int data = 0; data <= 15; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case STAINED_GLASS:
                    for(int data = 0; data <= 15; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case STAINED_GLASS_PANE:
                    for(int data = 0; data <= 15; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case PRISMARINE:
                    for(int data = 0; data <= 2; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case CARPET:
                    for(int data = 0; data <= 15; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case DOUBLE_PLANT:
                    for(int data = 0; data <= 5; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case RED_SANDSTONE:
                    for(int data = 0; data <= 2; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case SMOOTH_BRICK:
                    for(int data = 0; data <= 3; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case WOOD_STEP:
                    for(int data = 0; data <= 5; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case COBBLE_WALL:
                    for(int data = 0; data <= 1; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case COAL:
                    for(int data = 0; data <= 1; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case GOLDEN_APPLE:
                    for(int data = 0; data <= 1; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case COOKED_FISH:
                    for(int data = 0; data <= 1; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case RAW_FISH:
                    for(int data = 0; data <= 3; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case INK_SACK:
                    for(int data = 0; data <= 15; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case MONSTER_EGG:
                    for(int data = 50; data <= 120; data++){
                        if(data == 53 || data == 63 || data == 64 || (data >= 69 && data <= 89) || data == 97 || data == 99 || (data >= 102 && data <= 119)){
                            continue;
                        }
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case STEP:
                    for(int data = 0; data <= 7; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case WOOL:
                    for(int data = 0; data <= 15; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case RED_ROSE:
                    for(int data = 0; data <= 8; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                default:
                    itemStack = new ItemStack(material, 1, (short) 0);
                    toReturn.add(itemStack);
                    break;
            }
        }

        return toReturn;
    }
}
