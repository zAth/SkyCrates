package me.zath.skycrates.utils;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.Config;
import me.zath.skycrates.SkyCrates;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Utils {

    private Config config;
    private GuiItems guiItems;
    private Reflection reflection;
    private Serializer serializer;

    public Serializer getSerializer() {
        if(serializer == null)
            serializer = new Serializer();

        return serializer;
    }

    public Config getConfig() {
        if(config == null)
            config = new Config();

        return config;
    }

    public GuiItems getGuiItems() {
        if(guiItems == null)
            guiItems = new GuiItems();

        return guiItems;
    }

    public Reflection getReflection() {
        if(reflection == null)
            reflection = new Reflection();

        return reflection;
    }

    public void reload(){
        config = new Config();
        guiItems = new GuiItems();
    }

    public Player getPlayer(String playerName){
        for(Player player : SkyCrates.getSkyCrates().getServer().getOnlinePlayers())
            if(player.getName().equalsIgnoreCase(playerName))
                return player;

        return null;
    }

    public void updateConfig(Plugin skyChests, String configGeral, String configPlugin){
        try{
            YamlConfiguration finalyml = new YamlConfiguration();
            try {
                finalyml.load(new File(skyChests.getDataFolder(), configGeral));
            } catch (Exception e) {
                e.printStackTrace();
            }
            FileConfiguration tempConfig = YamlConfiguration.loadConfiguration(skyChests.getResource(configPlugin));
            for(String key : tempConfig.getKeys(true)){
                Object obj = tempConfig.get(key);
                if(finalyml.get(key) != null) obj = finalyml.get(key);
                finalyml.set(key, obj);
            }
            finalyml.save(new File(skyChests.getDataFolder(), configGeral));
            finalyml.load(new File(skyChests.getDataFolder(), configGeral));
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public List<String> splitEqually(String text, int size) {
        List<String> toReturn = new ArrayList<>();
        String toAdd = "";

        for (String split : text.split(" ")) {
            if ((toAdd + split).length() > Math.min(size, text.length() - (toReturn.size() * size))) {
                toReturn.add(toAdd);
                toAdd = split + " ";
                continue;
            }

            toAdd += split + " ";
        }

        if (!toReturn.contains(toAdd)) toReturn.add(toAdd);

        return toReturn;
    }

    public void knockBack(Player player, Location location) {
        Vector vector = player.getLocation().toVector().subtract(location.toVector()).normalize().multiply(1).setY(.1);

        if(player.isInsideVehicle()) {
            player.getVehicle().setVelocity(vector);
            return;
        }

        player.setVelocity(vector);
    }

    public int[] getFillSlots(int rows){
        int[] toReturn = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

        switch (rows){
            case 3:
                for(int i = 17; i <= 26; i++){
                    toReturn = addInt(toReturn, i);
                }

                break;
            case 4:
                toReturn = addInt(toReturn, 17);
                toReturn = addInt(toReturn, 18);
                for(int i = 26; i <= 35; i++){
                    toReturn = addInt(toReturn, i);
                }

                break;
            case 5:
                toReturn = addInt(toReturn, 17);
                toReturn = addInt(toReturn, 18);
                toReturn = addInt(toReturn, 26);
                toReturn = addInt(toReturn, 27);
                for(int i = 35; i <= 45; i++){
                    toReturn = addInt(toReturn, i);
                }

                break;
            case 6:
                toReturn = addInt(toReturn, 17);
                toReturn = addInt(toReturn, 18);
                toReturn = addInt(toReturn, 26);
                toReturn = addInt(toReturn, 27);
                toReturn = addInt(toReturn, 35);
                toReturn = addInt(toReturn, 36);
                for(int i = 44; i <= 53; i++){
                    toReturn = addInt(toReturn, i);
                }

                break;
        }

        return toReturn;
    }

    private int[] addInt(int[] array, int toAdd) {
        array  = Arrays.copyOf(array, array.length + 1);
        array[array.length - 1] = toAdd;
        return array;
    }

    public Location toLocation(String string) {
        String[] splitted = string.split("_");

        World world = SkyCrates.getSkyCrates().getServer().getWorld(splitted[0]);
        int x = Integer.parseInt(splitted[1]);
        int y = Integer.parseInt(splitted[2]);
        int z = Integer.parseInt(splitted[3]);

        return new Location(world, x, y, z);
    }

    public String toString(Location location) {
        return location.getWorld().getName() + "_" + location.getBlockX() + "_" + location.getBlockY() + "_" + location.getBlockZ();
    }

    public int getProperSize(int size) {
        int toReturn = 9 * 6;

        if(size <= 28){
            toReturn = 9 * 6;

            if (size <= 21) {
                toReturn = 9 * 5;

                if (size <= 14) {
                    toReturn = 9 * 4;

                    if (size <= 7) {
                        toReturn = 9 * 3;
                    }
                }
            }
        }

        return toReturn;
    }

    public String removeBorderBlanks(String string){
        String toReturn = string;

        for(int index = 0; index < string.length(); index++){
            if(!Character.toString(string.charAt(index)).equals(" ")){
                toReturn = string.substring(index);
                break;
            }
        }

        for(int index = toReturn.length() - 1; index >= 0; index--){
            if(!Character.toString(toReturn.charAt(index)).equals(" ")){
                toReturn = toReturn.substring(0, index + 1);
                break;
            }
        }

        if(toReturn.length() != 0 && Character.toString(toReturn.charAt(0)).equals(" "))
            toReturn = "";

        return toReturn;
    }

    public void changeChestState(Location location, boolean open) {
        byte dataByte = (open) ? (byte) 1 : 0;
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.playNote(location, (byte) 1, dataByte);
            try {
                Object blockPosition = getReflection().getConstructor_BlockPosition().newInstance(location.getBlockX(), location.getBlockY(), location.getBlockZ());
                Object id = getReflection().getMethod_GetById().invoke(null, location.getBlock().getTypeId());
                Object packetPlayOutBlockAction = getReflection().getConstructor_PacketPlayOutBlockAction().newInstance(blockPosition
                    , id, (byte) 1, dataByte);

                getReflection().getMethod_SendPacket().invoke(getReflection().getConnection(player), packetPlayOutBlockAction);
            } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
                e.printStackTrace();
            }
        }
    }

    public String prettifyText(String ugly) {
        if (!ugly.contains("_") && (!ugly.equals(ugly.toUpperCase())))
            return ugly;

        String toReturn = "";
        ugly = ugly.toLowerCase();

        if (ugly.contains("_")) {
            String[] splitted = ugly.split("_");
            int index = 0;
            for (String string : splitted) {
                index++;
                toReturn += Character.toUpperCase(string.charAt(0)) + string.substring(1);
                if (index < splitted.length)
                    toReturn += " ";
            }
        } else {
            toReturn += Character.toUpperCase(ugly.charAt(0)) + ugly.substring(1);
        }

        return toReturn;
    }

    public boolean isInt(String string){
        try{
            Integer.parseInt(string);
            return true;
        } catch (NumberFormatException ex){
            return false;
        }
    }

    public boolean isBoolean(String string){
        try{
            Boolean.parseBoolean(string);
            return true;
        } catch (NumberFormatException ex){
            return false;
        }
    }
}
