package me.zath.skycrates.utils;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.objects.CrateType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

public class GuiHolder implements InventoryHolder {

    public enum TYPE {
        PREVIEW, EDIT, COMMAND, CHANCE, BROADCAST, KEY, CONFIRM
        , BASIC
    }

    private int page;
    private CrateType crateType;
    private TYPE type;

    public GuiHolder(TYPE type, CrateType crateType, int page) {
        this.type = type;
        this.crateType = crateType;
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public CrateType getCrateType() {
        return crateType;
    }

    public TYPE getType() {
        return type;
    }

    @Override
    public Inventory getInventory() {
        return null;
    }

}
