package me.zath.skycrates.utils;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class ItemBuilder {

    private ItemStack itemStack;

    public ItemBuilder(int id, int data) {
        this.itemStack = new ItemStack(id, 1, (short) data);
    }

    public ItemBuilder(ItemStack itemStack) {
        this.itemStack = itemStack.clone();
    }

    public ItemBuilder setIdData(int id, byte data) {
        this.itemStack.setDurability(data);
        this.itemStack.setTypeId(id);
        return this;
    }

    private ItemMeta getItemMeta() {
        return this.itemStack.getItemMeta();
    }

    private ItemBuilder setItemMeta(ItemMeta itemMeta) {
        this.itemStack.setItemMeta(itemMeta);
        return this;
    }

    public ItemBuilder setAmount(int amount) {
        this.itemStack.setAmount(amount);
        return this;
    }

    public ItemBuilder withName(String displayName) {
        ItemMeta itemMeta = getItemMeta();
        itemMeta.setDisplayName(displayName);
        return setItemMeta(itemMeta);
    }

    public ItemBuilder withLore(List<String> lore) {
        ItemMeta itemMeta = getItemMeta();
        itemMeta.setLore(lore);
        return setItemMeta(itemMeta);
    }

    public ItemBuilder withGlow(Boolean toggle) {
        if (toggle) {
            try {
                Object nmsItem = SkyCrates.getSkyCrates().getUtils().getReflection().getMethod_asNMSCopy().invoke(null, this.itemStack);
                Object nbtTagCompound = null;

                if(!(boolean) SkyCrates.getSkyCrates().getUtils().getReflection().getMethod_HasTag().invoke(nmsItem)){
                    nbtTagCompound = SkyCrates.getSkyCrates().getUtils().getReflection().getConstructor_NBTTagCompound().newInstance();
                    SkyCrates.getSkyCrates().getUtils().getReflection().getMethod_SetTag().invoke(nmsItem, nbtTagCompound);
                }

                if(nbtTagCompound == null){
                    nbtTagCompound = SkyCrates.getSkyCrates().getUtils().getReflection().getMethod_GetTag().invoke(nmsItem);
                }

                Object nbtTagList = SkyCrates.getSkyCrates().getUtils().getReflection().getConstructor_NBTTagList().newInstance();
                SkyCrates.getSkyCrates().getUtils().getReflection().getMethod_Set().invoke(nbtTagCompound, "ench", nbtTagList);
                SkyCrates.getSkyCrates().getUtils().getReflection().getMethod_SetTag().invoke(nmsItem, nbtTagCompound);

                this.itemStack = (ItemStack) SkyCrates.getSkyCrates().getUtils().getReflection().getMethod_AsCraftMirror().invoke(null, nmsItem);
            } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
                e.printStackTrace();
            }
        }

        return this;
    }

    public ItemBuilder withEnchant(Enchantment enchantment, int level) {
        this.itemStack.addUnsafeEnchantment(enchantment, level);
        return this;
    }

    public ItemStack build() {
        return this.itemStack;
    }
}
