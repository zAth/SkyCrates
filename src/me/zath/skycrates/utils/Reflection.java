package me.zath.skycrates.utils;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import org.bukkit.entity.Player;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Reflection {

    private String versionPrefix = "";
    private Class<?> class_ItemStack, class_NBTBase, class_NBTTagCompound, class_NBTTagList, class_CraftItemStack, class_NBTCompressedStreamTools
        , class_PlayerConnection, class_Packet, class_BlockPosition, class_PacketPlayOutBlockAction, class_Block;
    private Method method_asNMSCopy, method_SaveItem, method_Add, method_Save, method_A, method_CreateStack, method_AsBukkitCopy, method_HasTag
        , method_SetTag, method_GetTag, method_Set, method_AsCraftMirror, method_SendPacket, method_GetById;
    private Constructor<?> constructor_NBTTagCompound, constructor_NBTTagList, constructor_BlockPosition, constructor_PacketPlayOutBlockAction;

    public Reflection() {
        loadClasses();
        loadMethods();
        loadConstructors();
    }

    private void loadClasses() {
        try {
            String className = SkyCrates.getSkyCrates().getServer().getClass().getName();
            String[] packages = className.split("\\.");
            if (packages.length == 5) {
                versionPrefix = packages[3] + ".";
            }
            class_Block = fixBukkitClass("net.minecraft.server.Block");
            class_PacketPlayOutBlockAction = fixBukkitClass("net.minecraft.server.PacketPlayOutBlockAction");
            class_BlockPosition = fixBukkitClass("net.minecraft.server.BlockPosition");
            class_Packet = fixBukkitClass("net.minecraft.server.Packet");
            class_PlayerConnection = fixBukkitClass("net.minecraft.server.PlayerConnection");
            class_ItemStack = fixBukkitClass("net.minecraft.server.ItemStack");
            class_NBTBase = fixBukkitClass("net.minecraft.server.NBTBase");
            class_NBTTagCompound = fixBukkitClass("net.minecraft.server.NBTTagCompound");
            class_NBTTagList = fixBukkitClass("net.minecraft.server.NBTTagList");
            class_CraftItemStack = fixBukkitClass("org.bukkit.craftbukkit.inventory.CraftItemStack");
            class_NBTCompressedStreamTools = fixBukkitClass("net.minecraft.server.NBTCompressedStreamTools");
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    private void loadMethods() {
        try {
            method_GetById = class_Block.getMethod("getById", int.class);
            method_SendPacket = class_PlayerConnection.getMethod("sendPacket", class_Packet);
            method_asNMSCopy = getClass_CraftItemStack().getMethod("asNMSCopy", org.bukkit.inventory.ItemStack.class);
            method_SaveItem = getClass_ItemStack().getMethod("save", getClass_NBTTagCompound());
            method_Add = getClass_NBTTagList().getMethod("add", getClass_NBTBase());
            method_Save = getClass_NBTCompressedStreamTools().getMethod("a", getClass_NBTTagCompound(), DataOutput.class);
            method_A = getClass_NBTCompressedStreamTools().getMethod("a", DataInputStream.class);
            method_CreateStack = getClass_ItemStack().getMethod("createStack", getClass_NBTTagCompound());
            method_AsBukkitCopy = getClass_CraftItemStack().getMethod("asBukkitCopy", getClass_ItemStack());
            method_HasTag = class_ItemStack.getMethod("hasTag");
            method_SetTag = class_ItemStack.getMethod("setTag", class_NBTTagCompound);
            method_GetTag = class_ItemStack.getMethod("getTag");
            method_Set = class_NBTTagCompound.getMethod("set", String.class, class_NBTBase);
            method_AsCraftMirror = class_CraftItemStack.getMethod("asCraftMirror", class_ItemStack);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    private void loadConstructors() {
        try {
            constructor_NBTTagCompound = class_NBTTagCompound.getConstructor();
            constructor_NBTTagList = class_NBTTagList.getConstructor();
            constructor_BlockPosition = class_BlockPosition.getConstructor(int.class, int.class, int.class);
            constructor_PacketPlayOutBlockAction = class_PacketPlayOutBlockAction.getConstructor(class_BlockPosition, class_Block, int.class, int.class);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    private Class<?> fixBukkitClass(String className) {
        className = className.replace("org.bukkit.craftbukkit.", "org.bukkit.craftbukkit." + versionPrefix);
        className = className.replace("net.minecraft.server.", "net.minecraft.server." + versionPrefix);
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object getConnection(Player p) {
        try {
            Method method_GetHandle = p.getClass().getMethod("getHandle");
            Object nmsPlayer = method_GetHandle.invoke(p);
            Field field_PlayerConnection = nmsPlayer.getClass().getField("playerConnection");
            return field_PlayerConnection.get(nmsPlayer);
        } catch (Exception ignored) {
            return null;
        }
    }

    public String getVersionPrefix() {
        return versionPrefix;
    }

    public Class<?> getClass_PlayerConnection() {
        return class_PlayerConnection;
    }

    public Class<?> getClass_Packet() {
        return class_Packet;
    }

    public Class<?> getClass_BlockPosition() {
        return class_BlockPosition;
    }

    public Class<?> getClass_PacketPlayOutBlockAction() {
        return class_PacketPlayOutBlockAction;
    }

    public Method getMethod_GetById() {
        return method_GetById;
    }

    public Class<?> getClass_Block() {
        return class_Block;
    }

    public Constructor<?> getConstructor_PacketPlayOutBlockAction() {
        return constructor_PacketPlayOutBlockAction;
    }

    public Constructor<?> getConstructor_BlockPosition() {
        return constructor_BlockPosition;
    }

    public Method getMethod_SendPacket() {
        return method_SendPacket;
    }

    public Method getMethod_GetTag() {
        return method_GetTag;
    }

    public Method getMethod_Set() {
        return method_Set;
    }

    public Method getMethod_AsCraftMirror() {
        return method_AsCraftMirror;
    }

    public Method getMethod_SetTag() {
        return method_SetTag;
    }

    public Method getMethod_HasTag() {
        return method_HasTag;
    }

    public Method getMethod_AsBukkitCopy() {
        return method_AsBukkitCopy;
    }

    public Method getMethod_CreateStack() {
        return method_CreateStack;
    }

    public Method getMethod_A() {
        return method_A;
    }

    public Method getMethod_asNMSCopy() {
        return method_asNMSCopy;
    }

    public Method getMethod_SaveItem() {
        return method_SaveItem;
    }

    public Method getMethod_Add() {
        return method_Add;
    }

    public Method getMethod_Save() {
        return method_Save;
    }

    public Constructor<?> getConstructor_NBTTagList() {
        return constructor_NBTTagList;
    }

    public Constructor<?> getConstructor_NBTTagCompound() {
        return constructor_NBTTagCompound;
    }

    private Class<?> getClass_ItemStack() {
        return class_ItemStack;
    }

    private Class<?> getClass_NBTBase() {
        return class_NBTBase;
    }

    public Class<?> getClass_NBTTagCompound() {
        return class_NBTTagCompound;
    }

    public Class<?> getClass_NBTTagList() {
        return class_NBTTagList;
    }

    private Class<?> getClass_CraftItemStack() {
        return class_CraftItemStack;
    }

    private Class<?> getClass_NBTCompressedStreamTools() {
        return class_NBTCompressedStreamTools;
    }
}
