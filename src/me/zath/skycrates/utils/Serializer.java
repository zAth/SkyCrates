package me.zath.skycrates.utils;
/*
 * MC
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.bukkit.inventory.ItemStack;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.math.BigInteger;

public class Serializer {

    public Serializer() {
    }

    public String toBase64(ItemStack itemStack) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutput = new DataOutputStream(outputStream);

        try {
            Object nbtTagListItems = SkyCrates.getSkyCrates().getUtils().getReflection().getClass_NBTTagList().newInstance();
            Object nbtTagCompoundItem = SkyCrates.getSkyCrates().getUtils().getReflection().getClass_NBTTagCompound().newInstance();

            Object nms = SkyCrates.getSkyCrates().getUtils().getReflection().getMethod_asNMSCopy().invoke(null, itemStack);

            SkyCrates.getSkyCrates().getUtils().getReflection().getMethod_SaveItem().invoke(nms, nbtTagCompoundItem);

            SkyCrates.getSkyCrates().getUtils().getReflection().getMethod_Add().invoke(nbtTagListItems, nbtTagCompoundItem);

            SkyCrates.getSkyCrates().getUtils().getReflection().getMethod_Save().invoke(null, nbtTagCompoundItem, dataOutput);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }

        return new BigInteger(1, outputStream.toByteArray()).toString(32);
    }

    public ItemStack fromBase64(String data) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(new BigInteger(data, 32).toByteArray());
        Object nbtTagCompoundRoot;
        Object nmsItem = null;
        Object toReturn = null;

        try {
            nbtTagCompoundRoot = SkyCrates.getSkyCrates().getUtils().getReflection().getMethod_A().invoke(null, new DataInputStream(inputStream));

            if (nbtTagCompoundRoot != null) {
                nmsItem = SkyCrates.getSkyCrates().getUtils().getReflection().getMethod_CreateStack().invoke(null, nbtTagCompoundRoot);
            }

            toReturn = SkyCrates.getSkyCrates().getUtils().getReflection().getMethod_AsBukkitCopy().invoke(null, nmsItem);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }

        return (ItemStack) toReturn;
    }

}