package me.zath.skycrates.listeners;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.inventories.ConfirmGUI;
import me.zath.skycrates.inventories.EditGUI;
import me.zath.skycrates.managers.CrateManager;
import me.zath.skycrates.objects.Crate;
import me.zath.skycrates.objects.CrateType;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class BlockListener implements Listener {

    @EventHandler
    public void onClick(PlayerInteractEvent event) {
        CrateManager crateManager = SkyCrates.getSkyCrates().getCrateManager();

        if (event.getPlayer().getItemInHand() != null
            && (crateManager.isKey(event.getPlayer().getItemInHand())
            || crateManager.isVirtualItem(event.getPlayer().getItemInHand()))) {
            event.setCancelled(true);
        }

        if ((event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_BLOCK)
            && crateManager.isCrate(event.getClickedBlock().getLocation())) {
            event.setCancelled(true);

            Crate crate = crateManager.getCrate(event.getClickedBlock().getLocation());
            if (event.getPlayer().getItemInHand() != null && event.getPlayer().getItemInHand().getType() == Material.BLAZE_ROD &&
                event.getPlayer().hasPermission("skycrates.admin")) {
                if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
                    if (crate.isOccupied())
                        crate.getAnimation().end();

                    crateManager.destroyCrate(crate);
                    event.getClickedBlock().setType(Material.AIR);
                } else if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    event.getPlayer().openInventory(new EditGUI(crate.getCrateType()).getInventory());
                    event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.CHEST_OPEN, 1, 1);
                }
            } else if (event.getPlayer().getItemInHand() != null &&
                event.getPlayer().getItemInHand().isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getKey(crate.getCrateType().getName()))) {
                if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
                    event.getPlayer().openInventory(crate.getCrateType().getInventory());
                    event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.CHEST_OPEN, 1, 1);
                } else if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    if (crate.isOccupied()) {
                        event.getPlayer().sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_Occupied()
                            .replaceAll("%occupier%", crate.getOpener().getName()));
                        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.BAT_HURT, 1, 1);
                    } else {
                        if (event.getPlayer().getItemInHand().getAmount() > 1) {
                            event.getPlayer().getItemInHand().setAmount(event.getPlayer().getItemInHand().getAmount() - 1);
                        } else {
                            event.getPlayer().setItemInHand(null);
                        }
                        crate.open(event.getPlayer());
                        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.CHEST_OPEN, 1, 1);
                    }
                }
            } else {
                if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
                    event.getPlayer().openInventory(crate.getCrateType().getInventory());
                    event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.CHEST_OPEN, 1, 1);
                } else if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    SkyCrates.getSkyCrates().getUtils().knockBack(event.getPlayer(), event.getClickedBlock().getLocation());
                    event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.BAT_DEATH, 1, 1);
                }
            }
        } else if(SkyCrates.getSkyCrates().getCrateManager().isVirtualItem(event.getPlayer().getItemInHand())){
            CrateType crateType = crateManager.getCrateType(event.getPlayer().getItemInHand());
            if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                event.getPlayer().openInventory(new ConfirmGUI(crateType).getInventory());
                event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.CHEST_OPEN, 1, 1);
            } else {
                event.getPlayer().openInventory(crateType.getInventory());
                event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.CHEST_OPEN, 1, 1);
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if (!SkyCrates.getSkyCrates().getCrateManager().isCrate(event.getBlock().getLocation())) return;

        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event) {
        if (!SkyCrates.getSkyCrates().getCrateManager().isOpening(event.getPlayer())) return;
        Crate crate = SkyCrates.getSkyCrates().getCrateManager().getCrate(event.getPlayer());

        crate.getAnimation().end();
    }

}
