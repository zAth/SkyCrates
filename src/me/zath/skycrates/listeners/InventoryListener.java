package me.zath.skycrates.listeners;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.inventories.ChangeGUI;
import me.zath.skycrates.inventories.EditGUI;
import me.zath.skycrates.objects.Crate;
import me.zath.skycrates.objects.CrateType;
import me.zath.skycrates.objects.Prize;
import me.zath.skycrates.objects.SignGUI;
import me.zath.skycrates.utils.GuiHolder;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InventoryListener implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getInventory().getHolder() instanceof GuiHolder) {
            if (event.getSlotType() == InventoryType.SlotType.OUTSIDE || event.getCurrentItem() == null
                || event.getCurrentItem().getType() == Material.AIR)
                return;

            if (event.getCurrentItem().isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getFill())) {
                event.setCancelled(true);
                return;
            }

            if (event.getRawSlot() < event.getInventory().getSize()) {
                Player player = (Player) event.getWhoClicked();
                ItemStack itemStack = event.getCurrentItem();

                GuiHolder holder = ((GuiHolder) event.getInventory().getHolder());
                GuiHolder.TYPE type = holder.getType();
                CrateType crateType = holder.getCrateType();

                if (type == GuiHolder.TYPE.PREVIEW || type == GuiHolder.TYPE.BASIC) {
                    event.setCancelled(true);
                } else if (type == GuiHolder.TYPE.EDIT) {
                    if (itemStack.isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getName())) {
                        event.setCancelled(true);
                        String[] lines = new String[]{crateType.getName(), "", "", ""};
                        new SignGUI(SkyCrates.getSkyCrates()).open(player, lines, (_player, _lines) -> {
                            SkyCrates.getSkyCrates().getCrateManager().changeName(crateType, _lines[0].replaceAll("\"", ""));
                            crateType.updatePreviewGui();

                            player.openInventory(new EditGUI(crateType).getInventory());
                            player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                        });
                    } else if (itemStack.isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getAnimation())) {
                        event.setCancelled(true);
                        String[] lines = new String[]{crateType.getAnimationName(), "", "", ""};
                        new SignGUI(SkyCrates.getSkyCrates()).open(player, lines, (_player, _lines) -> {
                            if (SkyCrates.getSkyCrates().getCrateManager().isAnimation(_lines[0].replaceAll("\"", ""))) {
                                SkyCrates.getSkyCrates().getCrateManager().changeAnimation(crateType, _lines[0].replaceAll("\"", ""));
                                crateType.updatePreviewGui();
                            }

                            player.openInventory(new EditGUI(crateType).getInventory());
                            player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                        });
                    } else if (itemStack.isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getChangeNoKey())) {
                        event.setCancelled(true);
                        player.closeInventory();

                        Inventory inventory = SkyCrates.getSkyCrates().getCrateManager().getIconGUI().getInventory(0, crateType);
                        player.openInventory(inventory);
                        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                    } else if (itemStack.isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getCommand())) {
                        event.setCancelled(true);
                        player.closeInventory();

                        player.openInventory(new ChangeGUI(crateType, GuiHolder.TYPE.COMMAND).getInventory());
                        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                    } else if (itemStack.isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getChance())) {
                        event.setCancelled(true);
                        player.closeInventory();

                        player.openInventory(new ChangeGUI(crateType, GuiHolder.TYPE.CHANCE).getInventory());
                        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                    } else if (itemStack.isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getBroadcast())) {
                        event.setCancelled(true);
                        player.closeInventory();

                        player.openInventory(new ChangeGUI(crateType, GuiHolder.TYPE.BROADCAST).getInventory());
                        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                    } else if (itemStack.isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getDelete())) {
                        event.setCancelled(true);
                        player.closeInventory();
                        SkyCrates.getSkyCrates().getCrateManager().deleteCrateType(crateType);
                        player.playSound(player.getLocation(), Sound.CHEST_CLOSE, 1, 1);
                    }
                } else if (type == GuiHolder.TYPE.COMMAND) {
                    event.setCancelled(true);
                    Prize prize = crateType.getPrize(itemStack);
                    if (prize == null) return;

                    String command = prize.getType() == Prize.TYPE.CMD ? prize.getCmd() : "";
                    String[] lines = new String[]{"", "", "", ""};
                    List<String> dividedCommand = SkyCrates.getSkyCrates().getUtils().splitEqually(command, 16);
                    for (int i = 0; i < dividedCommand.size(); i++) {
                        lines[i] = dividedCommand.get(i);
                    }

                    new SignGUI(SkyCrates.getSkyCrates()).open(player, lines, (_player, _lines) -> {
                        String _command = String.join("", Stream.of(_lines).collect(Collectors.toList())).replaceAll("\"", "");
                        _command = SkyCrates.getSkyCrates().getUtils().removeBorderBlanks(_command);

                        if (_command.length() == 0) {
                            prize.setCmd("");
                            prize.setType(Prize.TYPE.ITEM);
                        } else {
                            prize.setCmd(_command);
                            prize.setType(Prize.TYPE.CMD);
                        }
                        SkyCrates.getSkyCrates().getCrateManager().changePrizeCommand(crateType, prize);

                        player.openInventory(new EditGUI(crateType).getInventory());
                        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                    });
                } else if (type == GuiHolder.TYPE.CHANCE) {
                    event.setCancelled(true);
                    Prize prize = crateType.getPrize(itemStack);
                    if (prize == null) return;

                    int chance = crateType.getPrizes().get(prize);

                    String[] lines = new String[]{"" + chance, "", "", ""};
                    new SignGUI(SkyCrates.getSkyCrates()).open(player, lines, (_player, _lines) -> {
                        if (SkyCrates.getSkyCrates().getUtils().isInt(_lines[0].replaceAll("\"", ""))) {
                            crateType.getPrizes().put(prize, Integer.parseInt(_lines[0].replaceAll("\"", "")));
                            SkyCrates.getSkyCrates().getCrateManager().changePrizeChance(crateType, prize);

                            crateType.updatePreviewGui();
                        }

                        player.openInventory(new EditGUI(crateType).getInventory());
                        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                    });
                } else if (type == GuiHolder.TYPE.BROADCAST) {
                    event.setCancelled(true);
                    Prize prize = crateType.getPrize(itemStack);
                    if (prize == null) return;

                    boolean broadcast = prize.isBroadcast();

                    String[] lines = new String[]{"" + broadcast, "", "", ""};
                    new SignGUI(SkyCrates.getSkyCrates()).open(player, lines, (_player, _lines) -> {
                        if (SkyCrates.getSkyCrates().getUtils().isBoolean(_lines[0].replaceAll("\"", ""))) {
                            boolean _broadcast = Boolean.parseBoolean(_lines[0].replaceAll("\"", ""));

                            prize.setBroadcast(_broadcast);
                            SkyCrates.getSkyCrates().getCrateManager().setBroadcastable(crateType, prize, _broadcast);
                        }

                        player.openInventory(new EditGUI(crateType).getInventory());
                        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                    });
                } else if (type == GuiHolder.TYPE.KEY) {
                    event.setCancelled(true);
                    if (itemStack.isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getChangeKey())) {
                        event.setCancelled(true);
                        SkyCrates.getSkyCrates().getCrateManager().setVirtualCrate(crateType, null);

                        player.openInventory(new EditGUI(crateType).getInventory());
                        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                    } else if (itemStack.isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getWriteIdData())) {
                        event.setCancelled(true);
                        String[] lines = crateType.isVirtual()
                            ? new String[]{"" + crateType.getVirtualItem().getTypeId(), "^^ ID ^^", "vv DATA vv", "" + crateType.getVirtualItem().getData().getData()}
                            : new String[]{"", "^^ ID ^^", "vv DATA vv", ""};
                        new SignGUI(SkyCrates.getSkyCrates()).open(player, lines, (_player, _lines) -> {
                            if (SkyCrates.getSkyCrates().getUtils().isInt(_lines[0].replaceAll("\"", ""))
                                && SkyCrates.getSkyCrates().getUtils().isInt(_lines[3].replaceAll("\"", ""))) {

                                int id = Integer.parseInt(_lines[0].replaceAll("\"", ""));
                                int data = Integer.parseInt(_lines[3].replaceAll("\"", ""));
                                ItemStack virtualItem = SkyCrates.getSkyCrates().getUtils().getGuiItems().getVirtualCrate(crateType.getName()
                                    , id, data);
                                SkyCrates.getSkyCrates().getCrateManager().setVirtualCrate(crateType, virtualItem);
                            }

                            player.openInventory(new EditGUI(crateType).getInventory());
                            player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                        });
                    } else if (itemStack.isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getPreviousPage())) {
                        event.setCancelled(true);
                        player.closeInventory();

                        int page = holder.getPage();
                        Inventory inventory = SkyCrates.getSkyCrates().getCrateManager().getIconGUI().getInventory(page - 1, crateType);
                        player.openInventory(inventory);
                        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                    } else if (itemStack.isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getNextPage())) {
                        event.setCancelled(true);
                        player.closeInventory();

                        int page = holder.getPage();
                        Inventory inventory = SkyCrates.getSkyCrates().getCrateManager().getIconGUI().getInventory(page + 1, crateType);
                        player.openInventory(inventory);
                        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                    } else {
                        event.setCancelled(true);
                        int id = itemStack.getTypeId();
                        int data = itemStack.getData().getData();
                        ItemStack virtualItem = SkyCrates.getSkyCrates().getUtils().getGuiItems().getVirtualCrate(crateType.getName()
                            , id, data);
                        SkyCrates.getSkyCrates().getCrateManager().setVirtualCrate(crateType, virtualItem);

                        player.openInventory(new EditGUI(crateType).getInventory());
                        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                    }
                } else if (type == GuiHolder.TYPE.CONFIRM) {
                    event.setCancelled(true);
                    if (itemStack.isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getOpen())) {
                        if(player.getItemInHand() == null || !SkyCrates.getSkyCrates().getCrateManager().isVirtualItem(player.getItemInHand())
                            || !SkyCrates.getSkyCrates().getCrateManager().getCrateType(player.getItemInHand()).equals(holder.getCrateType())){
                            player.sendMessage(SkyCrates.getSkyCrates().getUtils().getConfig().getMsg_HoldVirtualItem());
                            return;
                        }

                        event.setCancelled(true);
                        Crate crate = new Crate(crateType, player.getLocation());

                        if (player.getItemInHand().getAmount() > 1) {
                            player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
                        } else {
                            player.setItemInHand(null);
                        }

                        crate.open(player);
                        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                    } else if (itemStack.isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getClose())) {
                        event.setCancelled(true);

                        player.closeInventory();
                        player.playSound(player.getLocation(), Sound.CHEST_CLOSE, 1, 1);
                    } else if (itemStack.isSimilar(crateType.getVirtualItem())) {
                        event.setCancelled(true);

                        player.openInventory(crateType.getInventory());
                        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (event.getInventory().getHolder() instanceof GuiHolder) {
            GuiHolder holder = ((GuiHolder) event.getInventory().getHolder());
            if (holder.getType() != GuiHolder.TYPE.EDIT) return;

            CrateType crateType = holder.getCrateType();

            for (int slot = 10; slot < 44; slot++) {
                if (event.getInventory().getItem(slot) == null || event.getInventory().getItem(slot).getType() == Material.AIR
                    || event.getInventory().getItem(slot).isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getFill()))
                    continue;

                Prize prize = crateType.getPrize(event.getInventory().getItem(slot));
                if (prize == null) {
                    Prize newPrize = new Prize(event.getInventory().getItem(slot), false);

                    crateType.getPrizes().put(newPrize, 0);
                    SkyCrates.getSkyCrates().getCrateManager().createPrize(crateType, newPrize);
                }
            }

            List<Prize> prizesToRemove = new ArrayList<>();
            for (Prize prize : crateType.getPrizes().keySet()) {
                boolean removed = true;
                for (int slot = 10; slot < 44; slot++) {
                    if (event.getInventory().getItem(slot) == null || event.getInventory().getItem(slot).getType() == Material.AIR
                        || event.getInventory().getItem(slot).isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getFill()))
                        continue;

                    if (event.getInventory().getItem(slot).isSimilar(prize.getItemStack()))
                        removed = false;
                }
                if (removed) {
                    prizesToRemove.add(prize);
                }
            }

            prizesToRemove.forEach(prize -> SkyCrates.getSkyCrates().getCrateManager().deletePrize(crateType, prize));
            crateType.updatePreviewGui();
        }
    }
}
