package me.zath.skycrates.inventories;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.objects.CrateType;
import me.zath.skycrates.utils.GuiHolder;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;

public class IconGUI {

    private HashMap<Integer, Inventory> inventorysMap = new HashMap<>();

    public IconGUI() {
        Inventory inventory = SkyCrates.getSkyCrates().getServer().createInventory(null, 6 * 9);

        int[] fillSlots = SkyCrates.getSkyCrates().getUtils().getFillSlots(6);
        for (int fillSlot : fillSlots) {
            inventory.setItem(fillSlot, SkyCrates.getSkyCrates().getUtils().getGuiItems().getFill());
        }

        inventory.setItem(48, SkyCrates.getSkyCrates().getUtils().getGuiItems().getChangeKey());
        inventory.setItem(50, SkyCrates.getSkyCrates().getUtils().getGuiItems().getWriteIdData());
        int itemsPerPage = 0;
        for (int slot = 0; slot < inventory.getSize(); slot++) {
            if (inventory.getItem(slot) == null || inventory.getItem(slot).getType() == Material.AIR) {
                itemsPerPage++;
            }
        }

        ArrayList<ItemStack> availableIcons = SkyCrates.getSkyCrates().getUtils().getGuiItems().getIcons();

        Double inventorys = (availableIcons.size() / itemsPerPage) + 0.5;
        int maxPages = inventorys.intValue() + 1;

        for (int page = 0; page < maxPages; page++) {
            Inventory _inventory = SkyCrates.getSkyCrates().getServer().createInventory(null, 6 * 9);
            _inventory.setContents(inventory.getContents());

            int inicio = page * itemsPerPage;
            int fim = (page * itemsPerPage) + itemsPerPage;
            int slot = 0;

            for (ItemStack itemStack : availableIcons) {
                if (slot >= inicio && slot <= fim) {
                    _inventory.addItem(itemStack);
                }
                slot++;
            }

            if(page != 0){
                _inventory.setItem(46, SkyCrates.getSkyCrates().getUtils().getGuiItems().getPreviousPage());
            }
            if(page != maxPages){
                _inventory.setItem(52, SkyCrates.getSkyCrates().getUtils().getGuiItems().getNextPage());
            }

            inventorysMap.put(page, _inventory);
        }

    }

    public Inventory getInventory(int page, CrateType crateType) {
        Inventory inventory = SkyCrates.getSkyCrates().getServer().createInventory(
            new GuiHolder(GuiHolder.TYPE.KEY, crateType, page), 54, SkyCrates.getSkyCrates().getUtils().getConfig().getGui_PreviewName().replaceAll("%crateName%", crateType.getName()));

        inventory.setContents(inventorysMap.get(page).getContents());

        return inventory;
    }

}
