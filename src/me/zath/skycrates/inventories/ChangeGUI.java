package me.zath.skycrates.inventories;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.objects.CrateType;
import me.zath.skycrates.utils.GuiHolder;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ChangeGUI {

    private Inventory inventory;

    public ChangeGUI(CrateType crateType, GuiHolder.TYPE guiHolderType) {
        if(guiHolderType != GuiHolder.TYPE.COMMAND && guiHolderType != GuiHolder.TYPE.CHANCE)
            inventory = new EditGUI(crateType).getInventory();

        inventory = SkyCrates.getSkyCrates().getServer().createInventory(
            new GuiHolder(guiHolderType, crateType, 0), 54, SkyCrates.getSkyCrates().getUtils().getConfig().getGui_PreviewName().replaceAll("%crateName%", crateType.getName()));

        int[] fillSlots = SkyCrates.getSkyCrates().getUtils().getFillSlots(6);
        for (int fillSlot : fillSlots) {
            inventory.setItem(fillSlot, new ItemStack(Material.BARRIER));
        }

        crateType.getPrizes().keySet().forEach(prize -> inventory.addItem(prize.getItemStack()));

        for (int fillSlot : fillSlots) {
            inventory.setItem(fillSlot, SkyCrates.getSkyCrates().getUtils().getGuiItems().getFill());
        }
    }

    public Inventory getInventory() {
        return inventory;
    }
}
