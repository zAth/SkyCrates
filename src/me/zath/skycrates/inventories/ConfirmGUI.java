package me.zath.skycrates.inventories;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.objects.CrateType;
import me.zath.skycrates.utils.GuiHolder;
import org.bukkit.inventory.Inventory;

public class ConfirmGUI {

    private Inventory inventory;

    public ConfirmGUI(CrateType crateType) {
        inventory = SkyCrates.getSkyCrates().getServer().createInventory(
            new GuiHolder(GuiHolder.TYPE.CONFIRM, crateType, 0), 4 * 9, SkyCrates.getSkyCrates().getUtils().getConfig().getGui_PreviewName().replaceAll("%crateName%", crateType.getName()));

        inventory.setItem(13, crateType.getVirtualItem());
        inventory.setItem(20, SkyCrates.getSkyCrates().getUtils().getGuiItems().getOpen());
        inventory.setItem(24, SkyCrates.getSkyCrates().getUtils().getGuiItems().getClose());
    }

    public Inventory getInventory() {
        return inventory;
    }
}
