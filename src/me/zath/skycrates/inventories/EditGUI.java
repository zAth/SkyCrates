package me.zath.skycrates.inventories;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.objects.CrateType;
import me.zath.skycrates.utils.GuiHolder;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class EditGUI {

    private Inventory inventory;

    public EditGUI(CrateType crateType) {
        inventory = SkyCrates.getSkyCrates().getServer().createInventory(
            new GuiHolder(GuiHolder.TYPE.EDIT, crateType, 0), 54, SkyCrates.getSkyCrates().getUtils().getConfig().getGui_PreviewName().replaceAll("%crateName%", crateType.getName()));

        int[] fillSlots = SkyCrates.getSkyCrates().getUtils().getFillSlots(6);
        for (int fillSlot : fillSlots) {
            inventory.setItem(fillSlot, new ItemStack(Material.BARRIER));
        }

        crateType.getPrizes().keySet().forEach(prize -> inventory.addItem(prize.getItemStack()));

        for (int fillSlot : fillSlots) {
            inventory.setItem(fillSlot, SkyCrates.getSkyCrates().getUtils().getGuiItems().getFill());
        }

        inventory.setItem(45, SkyCrates.getSkyCrates().getUtils().getGuiItems().getName());
        inventory.setItem(46, SkyCrates.getSkyCrates().getUtils().getGuiItems().getChangeNoKey());
        inventory.setItem(47, SkyCrates.getSkyCrates().getUtils().getGuiItems().getAnimation());
        inventory.setItem(49, SkyCrates.getSkyCrates().getUtils().getGuiItems().getDelete());
        inventory.setItem(51, SkyCrates.getSkyCrates().getUtils().getGuiItems().getChance());
        inventory.setItem(52, SkyCrates.getSkyCrates().getUtils().getGuiItems().getBroadcast());
        inventory.setItem(53, SkyCrates.getSkyCrates().getUtils().getGuiItems().getCommand());
    }

    public Inventory getInventory() {
        return inventory;
    }
}
