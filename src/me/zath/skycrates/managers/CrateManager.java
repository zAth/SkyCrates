package me.zath.skycrates.managers;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skycrates.Config;
import me.zath.skycrates.SkyCrates;
import me.zath.skycrates.inventories.IconGUI;
import me.zath.skycrates.objects.Crate;
import me.zath.skycrates.objects.CrateType;
import me.zath.skycrates.objects.Prize;
import me.zath.skycrates.objects.animations.AbstractAnimation;
import me.zath.skycrates.objects.animations.BasicAnimation;
import me.zath.skycrates.utils.GuiHolder;
import me.zath.skycrates.utils.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CrateManager {

    private HashMap<String, CrateType> crateTypes;
    private HashMap<Location, Crate> crates;
    private HashMap<String, Class<?>> animations;
    private IconGUI iconGUI;

    public CrateManager() {
        loadAnimations();
        loadCrateTypes();
        loadCrates();
        iconGUI = new IconGUI();
    }

    public IconGUI getIconGUI() {
        return iconGUI;
    }

    private void loadAnimations() {
        animations = new HashMap<>();
        registerAnimation(BasicAnimation.class);
    }

    public void registerAnimation(Class<?> animation) {
        if (animations.containsKey(animation.getSimpleName().toLowerCase())) {
            SkyCrates.getSkyCrates().getServer().getConsoleSender().sendMessage("§4A animação §c" + animation.getClass().getSimpleName() + "§4não pode ser criada porque já existe uma com esse nome.");
            return;
        }

        animations.put(animation.getSimpleName().toLowerCase(), animation);

        loadCrateTypes();
        loadCrates();
    }

    public Set<String> getAnimationNames() {
        return animations.keySet();
    }

    public boolean isAnimation(String animationName) {
        return animations.containsKey(animationName.toLowerCase());
    }

    public AbstractAnimation getAnimation(String animationName, Crate crate) {
        try {
            Constructor<?> animationConstructor = animations.get(animationName.toLowerCase()).getConstructor(Crate.class);
            return (AbstractAnimation) animationConstructor.newInstance(crate);
        } catch (InvocationTargetException | NoSuchMethodException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void loadCrateTypes() {
        crateTypes = new HashMap<>();
        Config config = SkyCrates.getSkyCrates().getUtils().getConfig();
        for (String crateTypeKey : config.getConfigurationSection("CrateTypes").getKeys(false)) {
            String crateName = config.getString("CrateTypes." + crateTypeKey + ".Name").replaceAll("&", "§");

            String animationName = config.getString("CrateTypes." + crateTypeKey + ".AnimationType");
            if (!isAnimation(animationName))
                continue;

            HashMap<Prize, Integer> prizes = new HashMap<>();
            if (config.exists("CrateTypes." + crateTypeKey + ".Prizes") && !config.getConfigurationSection("CrateTypes." + crateTypeKey + ".Prizes").getKeys(false).isEmpty()) {
                for (String prizeKey : config.getConfigurationSection("CrateTypes." + crateTypeKey + ".Prizes").getKeys(false)) {
                    int chance = config.getInt("CrateTypes." + crateTypeKey + ".Prizes." + prizeKey + ".Chance");
                    boolean broadcast = config.getBoolean("CrateTypes." + crateTypeKey + ".Prizes." + prizeKey + ".Broadcast");

                    ItemStack itemStack = SkyCrates.getSkyCrates().getUtils().getSerializer()
                        .fromBase64(config.getString("CrateTypes." + crateTypeKey + ".Prizes." + prizeKey + ".ItemStack"));
                    Prize prize;
                    if (config.exists("CrateTypes." + crateTypeKey + ".Prizes." + prizeKey + ".Cmd")) {
                        String cmd = config.getString("CrateTypes." + crateTypeKey + ".Prizes." + prizeKey + ".Cmd");
                        prize = new Prize(itemStack, broadcast, cmd);
                    } else {
                        prize = new Prize(itemStack, broadcast);
                    }

                    prizes.put(prize, chance);
                }
            }

            int inventorySize = SkyCrates.getSkyCrates().getUtils().getProperSize(prizes.size());
            Inventory inventory = SkyCrates.getSkyCrates().getServer().createInventory(
                new GuiHolder(GuiHolder.TYPE.PREVIEW, null, 0), inventorySize, config.getGui_PreviewName().replaceAll("%crateName%", crateName));

            int[] fillSlots = SkyCrates.getSkyCrates().getUtils().getFillSlots(inventorySize / 9);
            for (int fillSlot : fillSlots) {
                inventory.setItem(fillSlot, SkyCrates.getSkyCrates().getUtils().getGuiItems().getFill());
            }

            prizes.keySet().forEach(prize -> {
                List<String> lore = prize.getItemStack().hasItemMeta() ? prize.getItemStack().getItemMeta().hasLore() ?
                    prize.getItemStack().getItemMeta().getLore() : new ArrayList<>() : new ArrayList<>();
                lore.add(SkyCrates.getSkyCrates().getUtils().getConfig().getGui_IconChance().replaceAll("%chance%", "" + prizes.get(prize)));

                ItemStack itemStack = new ItemBuilder(prize.getItemStack())
                    .withLore(lore)
                    .build();

                inventory.addItem(itemStack);
            });

            ItemStack itemStack = null;
            if(config.exists("CrateTypes." + crateTypeKey + ".VirtualCrate")){
                String[] splitted = config.getString("CrateTypes." + crateTypeKey + ".VirtualCrate").split("_");
                int id = Integer.parseInt(splitted[0]);
                int data = Integer.parseInt(splitted[1]);
                itemStack = SkyCrates.getSkyCrates().getUtils().getGuiItems().getVirtualCrate(crateName, id, data);
            }

            CrateType crateType = new CrateType(crateName, prizes, inventory, animationName, itemStack);
            crateTypes.put(crateName.toLowerCase(), crateType);
        }
    }

    public CrateType createCrateType(String name, String animationName) {
        Config config = SkyCrates.getSkyCrates().getUtils().getConfig();

        Inventory inventory = SkyCrates.getSkyCrates().getServer().createInventory(
            new GuiHolder(GuiHolder.TYPE.PREVIEW, null, 0), SkyCrates.getSkyCrates().getUtils().getProperSize(0)
            , SkyCrates.getSkyCrates().getUtils().getConfig().getGui_PreviewName().replaceAll("%crateName%", name));

        CrateType crateType = new CrateType(name, new HashMap<>(), inventory, animationName, null);
        crateTypes.put(name.toLowerCase(), crateType);

        int key;
        if (!config.exists("CrateTypes") || config.getConfigurationSection("CrateTypes").getKeys(false).isEmpty())
            key = 0;
        else
            key = config.getConfigurationSection("CrateTypes").getKeys(false).size();

        config.set("CrateTypes." + key + ".Name", name);
        config.set("CrateTypes." + key + ".AnimationType", animationName);

        return crateType;
    }

    public void deleteCrateType(CrateType crateType) {
        List<Crate> cratesToRemove = crates.values().stream().filter(crate -> crate.getCrateType() == crateType).collect(Collectors.toList());
        cratesToRemove.forEach(crate -> {
            if (crate.isOccupied())
                crate.getAnimation().end();

            destroyCrate(crate);
            crate.getLocation().getBlock().setType(Material.AIR);
        });

        crateTypes.remove(crateType.getName().toLowerCase());

        Config config = SkyCrates.getSkyCrates().getUtils().getConfig();
        int keys = config.getConfigurationSection("CrateTypes").getKeys(false).size();
        for (String crateTypeKey : config.getConfigurationSection("CrateTypes").getKeys(false)) {
            String crateName = config.getString("CrateTypes." + crateTypeKey + ".Name").replaceAll("&", "§");

            if (!crateName.equals(crateType.getName()))
                continue;

            for (int key = Integer.parseInt(crateTypeKey); key < keys - 1; key++) {
                config.set("CrateTypes." + key, config.getConfigurationSection("CrateTypes." + (key + 1)));
            }
            config.set("CrateTypes." + (keys - 1), null);

            return;
        }
    }

    public void changeName(CrateType crateType, String name) {
        Config config = SkyCrates.getSkyCrates().getUtils().getConfig();
        if (config.exists("Crates") && config.getConfigurationSection("Crates") != null
            && !config.getConfigurationSection("Crates").getKeys(false).isEmpty()) {
            config.getConfigurationSection("Crates").getKeys(false).stream()
                .filter(crateLocation -> config.getString("Crates." + crateLocation).equalsIgnoreCase(crateType.getName()))
                .forEach(crateLocation -> config.set("Crates." + crateLocation, name));
        }

        crateTypes.remove(crateType.getName().toLowerCase());
        crateTypes.put(name.toLowerCase(), crateType);

        for (String crateTypeKey : config.getConfigurationSection("CrateTypes").getKeys(false)) {
            String crateName = config.getString("CrateTypes." + crateTypeKey + ".Name").replaceAll("&", "§");

            if (!crateName.equals(crateType.getName()))
                continue;

            config.set("CrateTypes." + crateTypeKey + ".Name", name);
            crateType.setName(name);
            return;
        }
    }

    public void setVirtualCrate(CrateType crateType, ItemStack itemStack) {
        Config config = SkyCrates.getSkyCrates().getUtils().getConfig();
        for (String crateTypeKey : config.getConfigurationSection("CrateTypes").getKeys(false)) {
            String crateName = config.getString("CrateTypes." + crateTypeKey + ".Name").replaceAll("&", "§");

            if (!crateName.equals(crateType.getName()))
                continue;

            if(itemStack == null) {
                config.set("CrateTypes." + crateTypeKey + ".VirtualCrate", null);
                crateType.setVirtualItem(null);
            } else {
                String string = itemStack.getTypeId() + "_" + itemStack.getDurability();
                config.set("CrateTypes." + crateTypeKey + ".VirtualCrate", string);
                crateType.setVirtualItem(itemStack);
            }
            break;
        }

        List<Crate> cratesToRemove = crates.values().stream().filter(crate -> crate.getCrateType() == crateType).collect(Collectors.toList());
        cratesToRemove.forEach(crate -> {
            if (crate.isOccupied())
                crate.getAnimation().end();

            destroyCrate(crate);
            crate.getLocation().getBlock().setType(Material.AIR);
        });
    }

    public void changeAnimation(CrateType crateType, String animationName) {
        Config config = SkyCrates.getSkyCrates().getUtils().getConfig();
        for (String crateTypeKey : config.getConfigurationSection("CrateTypes").getKeys(false)) {
            String crateName = config.getString("CrateTypes." + crateTypeKey + ".Name").replaceAll("&", "§");

            if (!crateName.equals(crateType.getName()))
                continue;

            config.set("CrateTypes." + crateTypeKey + ".AnimationType", animationName);
            crateType.setAnimationName(animationName);
            return;
        }
    }

    public CrateType getCrateType(String name) {
        return crateTypes.get(name.toLowerCase());
    }

    public List<CrateType> getCrateTypes() {
        return crateTypes.values().stream().collect(Collectors.toList());
    }

    public List<String> getCrateTypesNames() {
        return crateTypes.keySet().stream().collect(Collectors.toList());
    }

    public boolean isCrateType(String crateTypeName) {
        return crateTypes.containsKey(crateTypeName.toLowerCase());
    }

    public void createPrize(CrateType crateType, Prize prize) {
        Config config = SkyCrates.getSkyCrates().getUtils().getConfig();
        for (String crateTypeKey : config.getConfigurationSection("CrateTypes").getKeys(false)) {
            String crateName = config.getString("CrateTypes." + crateTypeKey + ".Name").replaceAll("&", "§");
            if (!crateName.equals(crateType.getName()))
                continue;

            int prizeKey = config.exists("CrateTypes." + crateTypeKey + ".Prizes") && !config.getConfigurationSection("CrateTypes." + crateTypeKey + ".Prizes").getKeys(false).isEmpty() ?
                config.getConfigurationSection("CrateTypes." + crateTypeKey + ".Prizes").getKeys(false).size() : 0;

            config.set("CrateTypes." + crateTypeKey + ".Prizes." + prizeKey + ".Chance", 0);
            config.set("CrateTypes." + crateTypeKey + ".Prizes." + prizeKey + ".Broadcast", false);

            String cmd = prize.getType() == Prize.TYPE.CMD ? prize.getCmd() : "";
            if (prize.getType() == Prize.TYPE.CMD)
                config.set("CrateTypes." + crateTypeKey + ".Prizes." + prizeKey + ".Cmd", cmd);

            config.set("CrateTypes." + crateTypeKey + ".Prizes." + prizeKey + ".ItemStack"
                , SkyCrates.getSkyCrates().getUtils().getSerializer().toBase64(prize.getItemStack()));
        }
    }

    public void deletePrize(CrateType crateType, Prize prize) {
        crateType.getPrizes().remove(prize);

        Config config = SkyCrates.getSkyCrates().getUtils().getConfig();
        for (String crateTypeKey : config.getConfigurationSection("CrateTypes").getKeys(false)) {
            String crateName = config.getString("CrateTypes." + crateTypeKey + ".Name").replaceAll("&", "§");
            if (!crateName.equals(crateType.getName()))
                continue;

            int prizeKeys = config.getConfigurationSection("CrateTypes." + crateTypeKey + ".Prizes").getKeys(false).size();
            for (String prizeKey : config.getConfigurationSection("CrateTypes." + crateTypeKey + ".Prizes").getKeys(false)) {
                if (!SkyCrates.getSkyCrates().getUtils().getSerializer().toBase64(prize.getItemStack())
                    .equals(config.getString("CrateTypes." + crateTypeKey + ".Prizes." + prizeKey + ".ItemStack")))
                    continue;

                for (int key = Integer.parseInt(prizeKey); key < prizeKeys - 1; key++) {
                    config.set("CrateTypes." + crateTypeKey + ".Prizes." + key
                        , config.getConfigurationSection("CrateTypes." + crateTypeKey + ".Prizes." + (key + 1)));
                }
                config.set("CrateTypes." + crateTypeKey + ".Prizes." + (prizeKeys - 1), null);

                return;
            }
        }
    }

    public void changePrizeChance(CrateType crateType, Prize prize) {
        Config config = SkyCrates.getSkyCrates().getUtils().getConfig();
        for (String crateTypeKey : config.getConfigurationSection("CrateTypes").getKeys(false)) {
            String crateName = config.getString("CrateTypes." + crateTypeKey + ".Name").replaceAll("&", "§");
            if (!crateName.equals(crateType.getName()))
                continue;

            for (String prizeKey : config.getConfigurationSection("CrateTypes." + crateTypeKey + ".Prizes").getKeys(false)) {
                if (!SkyCrates.getSkyCrates().getUtils().getSerializer().toBase64(prize.getItemStack())
                    .equals(config.getString("CrateTypes." + crateTypeKey + ".Prizes." + prizeKey + ".ItemStack")))
                    continue;

                config.set("CrateTypes." + crateTypeKey + ".Prizes." + prizeKey + ".Chance", crateType.getPrizes().get(prize));
            }
        }
    }

    public void changePrizeCommand(CrateType crateType, Prize prize) {
        Config config = SkyCrates.getSkyCrates().getUtils().getConfig();
        for (String crateTypeKey : config.getConfigurationSection("CrateTypes").getKeys(false)) {
            String crateName = config.getString("CrateTypes." + crateTypeKey + ".Name").replaceAll("&", "§");
            if (!crateName.equals(crateType.getName()))
                continue;

            for (String prizeKey : config.getConfigurationSection("CrateTypes." + crateTypeKey + ".Prizes").getKeys(false)) {
                if (!SkyCrates.getSkyCrates().getUtils().getSerializer().toBase64(prize.getItemStack())
                    .equals(config.getString("CrateTypes." + crateTypeKey + ".Prizes." + prizeKey + ".ItemStack")))
                    continue;

                Object object = prize.getType() == Prize.TYPE.CMD ? prize.getCmd() : null;
                config.set("CrateTypes." + crateTypeKey + ".Prizes." + prizeKey + ".Cmd", object);
            }
        }
    }

    public void setBroadcastable(CrateType crateType, Prize prize, boolean broadcast) {
        Config config = SkyCrates.getSkyCrates().getUtils().getConfig();
        for (String crateTypeKey : config.getConfigurationSection("CrateTypes").getKeys(false)) {
            String crateName = config.getString("CrateTypes." + crateTypeKey + ".Name").replaceAll("&", "§");
            if (!crateName.equals(crateType.getName()))
                continue;

            for (String prizeKey : config.getConfigurationSection("CrateTypes." + crateTypeKey + ".Prizes").getKeys(false)) {
                if (!SkyCrates.getSkyCrates().getUtils().getSerializer().toBase64(prize.getItemStack())
                    .equals(config.getString("CrateTypes." + crateTypeKey + ".Prizes." + prizeKey + ".ItemStack")))
                    continue;

                config.set("CrateTypes." + crateTypeKey + ".Prizes." + prizeKey + ".Broadcast", broadcast);
            }
        }
    }

    public Crate createCrate(Location location, CrateType crateType) {
        Crate crate = new Crate(crateType, location);
        crates.put(location, crate);

        SkyCrates.getSkyCrates().getUtils().getConfig().set("Crates." + SkyCrates.getSkyCrates().getUtils().toString(location), crateType.getName());

        return crate;
    }

    public void destroyCrate(Crate crate) {
        crates.remove(crate.getLocation());

        SkyCrates.getSkyCrates().getUtils().getConfig().set("Crates." + SkyCrates.getSkyCrates().getUtils().toString(crate.getLocation()), null);
    }

    public List<Crate> getCrates() {
        return crates.values().stream().collect(Collectors.toList());
    }

    public Crate getCrate(Location location) {
        return crates.get(location);
    }

    public boolean isCrate(Location location) {
        return crates.containsKey(location);
    }

    private void loadCrates() {
        crates = new HashMap<>();
        Config config = SkyCrates.getSkyCrates().getUtils().getConfig();
        if (!config.exists("Crates") || config.getConfigurationSection("Crates").getKeys(false).isEmpty()) return;
        for (String serializedLocation : config.getConfigurationSection("Crates").getKeys(false)) {
            Location location = SkyCrates.getSkyCrates().getUtils().toLocation(serializedLocation);

            String crateTypeName = config.getString("Crates." + serializedLocation);
            if (!isCrateType(crateTypeName.toLowerCase()))
                continue;
            CrateType crateType = crateTypes.get(crateTypeName.toLowerCase());

            Crate crate = new Crate(crateType, location);
            crates.put(location, crate);
        }
    }

    public boolean isOpening(Player player) {
        return crates.values().stream()
            .filter(Crate::isOccupied)
            .filter(crate -> crate.getOpener() == player)
            .findAny()
            .isPresent();
    }

    public Crate getCrate(Player player) {
        return crates.values().stream()
            .filter(Crate::isOccupied)
            .filter(crate -> crate.getOpener() == player)
            .findAny()
            .get();
    }

    public boolean isKey(ItemStack itemStack) {
        return crateTypes.values()
            .stream()
            .filter(crateType -> itemStack.isSimilar(SkyCrates.getSkyCrates().getUtils().getGuiItems().getKey(crateType.getName())))
            .findAny()
            .isPresent();
    }

    public boolean isVirtualItem(ItemStack itemStack) {
        return crateTypes.values()
            .stream()
            .filter(CrateType::isVirtual)
            .filter(crateType -> itemStack.isSimilar(crateType.getVirtualItem()))
            .findAny()
            .isPresent();
    }

    public CrateType getCrateType(ItemStack itemStack) {
        return crateTypes.values()
            .stream()
            .filter(CrateType::isVirtual)
            .filter(crateType -> itemStack.isSimilar(crateType.getVirtualItem()))
            .findAny()
            .get();
    }

}
